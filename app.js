var express = require('express');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var mongoose = require('mongoose');

var autoIncrement = require('mongoose-auto-increment');
var nodemailer = require("nodemailer");
var app = express();
var path = require('path');
var url = require("url");
var multer = require('multer');

var storage = multer.diskStorage({
    destination: './uploads/',
    filename: function (req, file, cb) {
        cb(null, file.originalname.replace(path.extname(file.originalname), "") + '-' + Date.now() + path.extname(file.originalname))
    }
})

var upload = multer({ storage: storage })

// configure app
app.use(express.static('./app'));
app.use('/bower_components', express.static(__dirname + '/bower_components'));
app.use('/Styles', express.static(__dirname + '/Styles/'));
app.use('/Scripts', express.static(__dirname + '/Scripts/'));
app.use('/plugins', express.static(__dirname + '/plugins'));
app.use('/dist', express.static(__dirname + '/dist'));
app.use('/images', express.static(__dirname + '/images'));
app.use(morgan('dev')); // log requests to the console
//app.use(express.static(__dirname + './images'));
// configure body parser
//app.use(bodyParser.urlencoded({ extended: true }));
//app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ limit: '50mb' }));

app.use(function (req, res, next) {
    
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
    res.setHeader("Pragma", "no-cache"); // HTTP 1.0.
    res.setHeader("Expires", "0"); 

    // Pass to next layer of middleware
    next();
});


// REGISTER OUR ROUTES -------------------------------
var users = require('./routes/Users');
var projects = require('./routes/Projects');
var environments = require('./routes/Environments');
var repositorys = require('./routes/Repositorys');
var resources = require('./routes/Resources');
var vm = require('./routes/VM');
var organization = require('./routes/Organization');
var common = require('./routes/Common');
var notification = require('./routes/Notification');
var interface = require('./routes/Interface');

app.use('/users', users);
app.use('/projects', projects);
app.use('/environments', environments);
app.use('/repositorys', repositorys);
app.use('/resources', resources);
app.use('/vm', vm);
app.use('/organization', organization);
app.use('/common', common);
app.use('/interface', interface);
app.use('/notification', notification);
//require('./routes/UserRepository')(app);

//get image path refers
app.use(express.static(__dirname + '/'));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    //  var err = new Error('Not Found');
    // err.status = 404;
    next();
});



var port = process.env.PORT || 3200; // set our port
var host = (process.env.HOST || 'localhost');

//mongoose.connect('mongodb://13.127.132.69/AddressBook');
// database
//var mongoose   = require('mongoose');

//mongoose.connect('mongodb://localhost:27017/CloudFlex'); // connect to our database
mongoose.connect('mongodb://admin:valholla123@13.127.235.145:27017/Valholla_Testing?authSource=admin'); // connect to our database

// CONNECTION EVENTS
// If the connection throws an error
mongoose.connection.on('error', function (err) {
    console.log('Mongoose default connection error: ' + err);
});

// When successfully connected
mongoose.connection.once('open', function () {
    console.log('Connection successful');
    // Start server
    app.listen(port);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
    console.log('Mongoose default connection disconnected');
});


// If the Node process ends, close the Mongoose connection
process.on('SIGINT', function () {
    mongoose.connection.close(function () {
        console.log('Mongoose default connection disconnected through app termination');
        process.exit(0);
    });
});

module.exports = app;

