var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var path = require('path');
var http = require('http');
var RegionDetailMaster = require('../models/RegionDetailSchema.js');
var VMTemplateMaster = require('../models/VMTemplateDetailSchema.js');
var UserMaster = require('../models/userschema.js');
var RoleMaster = require('../models/roleschema.js');
var EnvironmentMaster = require('../models/EnvironmentConfigureSchema.js');
var ProjectMaster = require('../models/projectschema.js');
var UserProjectRoleMaster = require('../models/userprojectroleschema.js');
var VMProjectEnvironmentConfigurationMaster = require('../models/VMProjectEnvironmentConfigurationSchema.js');
var deleteProject = require("./InstanceCRUD");
var manager = new deleteProject();
function getProjectDetails(req, res, next) {
    ProjectMaster.find({
        IsActive: true
    }).populate('_userId').exec(function (err, project) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            return next(err);
        }

        res.json(project); // return all projects in JSON format
    });
};

// get all projects
router.get('/api/getProjectDetails', function (req, res, next) {
    // use mongoose to get all projects in the database

    getProjectDetails(req, res, next);
});


// get project by Id
router.get('/api/getProjectById/:_id', function (req, res, next) {
    // use mongoose to get a project in the database    
    ProjectMaster.findOne({
        _id: req.params._id,
        IsActive: true
    }, function (err, project) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            return next(err);
        }

        res.json({
            IsSuccess: true,
            Message: "success",
            Data: project
        });
    });
});

// save project details
router.post('/api/saveProjectDetails', function (req, res, next) {
    if (req.body) {
        var objProject = {
            ProjectName: req.body.ProjectName,
            ProjectDescription: req.body.ProjectDescription,
            StartDate: req.body.StartDate,
            EndDate: req.body.EndDate,
            CreatedBy: req.body.CreatedBy,
            CreatedDate: req.body.CreatedDate,
            ModifiedBy: null,
            ModifiedDate: null,
            IsActive: true,
            _userId: req.body._userId,
        };

        if (req.body.ProjectId > 0) {
            ProjectMaster.findOne({
                _id: req.body.ProjectId,
                IsActive: true
            }, function (err, project) {
                if (err)
                    return next(err);
                if (!project) {
                    res.json({
                        IsSuccess: false,
                        Message: "Project does not exists"
                    });
                } else {
                    objProject.ProjectName = req.body.ProjectName;
                    objProject.ProjectDescription = req.body.ProjectDescription;
                    objProject.StartDate = req.body.StartDate;
                    objProject.EndDate = req.body.EndDate;
                    objProject.CreatedBy = project.CreatedBy;
                    objProject.CreatedDate = project.CreatedDate;
                    objProject.ModifiedBy = req.body.CreatedBy;
                    objProject.ModifiedDate = req.body.CreatedDate;
                    objProject._userId = req.body._userId;
                    ProjectMaster.update({
                        _id: req.body.ProjectId
                    }, {
                            $set: objProject
                        }).exec().then(function (resProject) {
                            if (err)
                                return next(err);
                            if (resProject) {

                                RoleMaster.findOne({
                                    RoleName: 'Project Manager'
                                }, function (err, objRole) {
                                    if (err)
                                        return next(err);
                                    if (objRole) {
                                        var projectUserRole = {
                                            _userId: req.body._userId,
                                            _roleId: objRole._id,
                                            _projectId: project._id,
                                            IsActive: true,
                                            IsPrimaryManager: true,
                                            CreatedBy: project.CreatedBy,
                                            CreatedDate: project.CreatedDate,
                                            ModifiedBy: req.body.CreatedBy,
                                            ModifiedDate: req.body.CreatedDate
                                        };

                                        UserProjectRoleMaster.update({
                                            _userId: project._userId,
                                            _projectId: project._id
                                        }, {
                                                $set: projectUserRole
                                            }).exec().then(function (result) {
                                                if (!result) {
                                                    res.json({
                                                        IsSuccess: false,
                                                        Message: "Project Name already exists"
                                                    });
                                                } else {
                                                    res.json({
                                                        IsSuccess: true,
                                                        Message: "Project has been updated successfully"
                                                    });
                                                }
                                            });
                                    }
                                });
                            }
                        });
                }

            });

        } else {

            ProjectMaster.create(objProject, function (err, project) {
                if (err)
                    return next(err);

                if (!project) {
                    res.json({
                        IsSuccess: false,
                        Message: "Project Name already exists"
                    });
                } else {
                    console.log('success');
                    RoleMaster.findOne({
                        RoleName: 'Project Manager'
                    }, function (err, role) {
                        if (err)
                            return next(err);
                        if (role) {
                            var projectUserRole = {
                                _userId: req.body._userId,
                                _roleId: role._id,
                                _projectId: project._id,
                                IsActive: true,
                                IsPrimaryManager: true,
                                CreatedBy: req.body.CreatedBy,
                                CreatedDate: req.body.CreatedDate,
                                ModifiedBy: null,
                                ModifiedDate: null,
                            };

                            UserProjectRoleMaster.create(projectUserRole, function (err, post) {
                                if (err)
                                    return next(err);
                                if (!post) {
                                    res.json({
                                        IsSuccess: false,
                                        Message: "Project Name already exists"
                                    });
                                } else {
                                    res.json({
                                        IsSuccess: true,
                                        Message: "New Project has been added successfully"
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }

    }


});


//update project details
router.post('/api/updateProjectDetails', function (req, res, next) {
    ProjectMaster.findOne({
        _id: req.body.ProjectId,
        IsActive: true
    }, function (err, project) {
        if (err)
            return next(err);
        if (!project) {
            res.json({
                IsSuccess: false,
                Message: "Project does not exists"
            });
        } else {
            var objProject = {
                ProjectName: req.body.ProjectName,
                ProjectDescription: req.body.ProjectDescription,
                StartDate: req.body.StartDate,
                EndDate: req.body.EndDate,
                CreatedBy: project.CreatedBy,
                CreatedDate: project.CreatedDate,
                ModifiedBy: req.body.CreatedBy,
                ModifiedDate: req.body.CreatedDate,
                _userId: project._userId
            };
            ProjectMaster.update({
                _id: req.body.ProjectId
            }, {
                    $set: objProject
                }).exec().then(function (resProject) {
                    if (err)
                        return next(err);
                    if (resProject) {
                        res.json({
                            IsSuccess: true,
                            Message: "Project has been updated successfully"
                        });
                    }
                });
        }

    });
});

// delete a Project
router.delete('/api/deleteProjectById/:_id', function (req, res) {
    console.log(req.params._id);
    ProjectMaster.update({
        _id: req.params._id
    }, {
            $set: {
                IsActive: false
            }
        }, function (err, result) {
           
            if (err)
                res.json({
                    IsSuccess: false,
                    Message: err,
                    Data: err
                });
            else {

                VMProjectEnvironmentConfigurationMaster.find({
                    IsActive: true,
                    _projectId: req.params._id
                }).populate('_projectId').populate('_cloudEnvironmentId').populate('_VMTemplateDetailId').populate('_environmentConfigureId').exec(function (err, projectVM) {
                    
                    // if there is an error retrieving, send the error. nothing after res.send(err) will execute
                    if (err) {
                        res.json({
                            IsSuccess: false,
                            Message: err,
                            Data: err
                        });
                    }
                    else {
                        manager.getRegionDetails(projectVM,req,res);
                        // res.json(projectVM); // return all projectVM in JSON format

                    }

                });
            }
        });
});



module.exports = router;