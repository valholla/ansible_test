
// JavaScript source code

var express = require('express');
var router = express.Router();
var nodemailer = require("nodemailer");
var mongoose = require('mongoose');
var fs = require('fs');
var path = require('path');
var http = require('http');
var lowerCase = require('lower-case');
var UserMaster = require('../models/userschema.js');
var RoleMaster = require('../models/roleschema.js');
var UserProjectRoleMaster = require('../models/userprojectroleschema.js');


var smtpTransport = nodemailer.createTransport({
    service: "gmail",
    host: "smtp.gmail.com",
    auth: {
        user: "cloudflex.help@gmail.com",
        pass: "Password@12345"
    }
});

function getRoleId(userId) {
    UserMaster.findOne({
        _id: userId,
        IsActive: true
    }, function (err, user) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.send(err);
        }
        console.log(user._roleId);
        return user._roleId;

    });

}

function getAllUsers(res) {
    UserMaster.find({
        IsActive: true
    }, function (err, users) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.send(users);
        }

        res.json(users); // return all projects in JSON format
    });

};

function getUser(req, res) {
    // console.log({$regex: new RegExp("^" + req.body.UserName.toLowerCase(), "i")})
    
    UserMaster.findOne({
        $or: [{
                UserName: {
                $regex: new RegExp("^" + req.body.UserName + "*$", "i")
                }
            },
            {
                Email: {
                $regex: new RegExp("^" + req.body.EmailId + "*$", "i")
                }
            }
        ],
        Password: req.body.Password,
        IsActive: req.body.IsActive
    }, function (err, user) {        
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
            //res.send(err);
        }
        if (!user) {
            res.json({
                IsSuccess: false,
                Message: 'Invalid Username/Email or password.',
                Data: null
            });
        } else {
            if (user.IsEmailActivated) {
                res.json({
                    IsSuccess: true,
                    Message: 'success',
                    Data: user
                });
            } else {
                res.json({
                    IsSuccess: false,
                    Message: 'Email not verified',
                    Data: user
                });
            }

        }

        // return all users in JSON format
    });
};

function getUserbyEmail(req, res) {
    // console.log(req.body.Email_lower);
    UserMaster.findOne({
        $or: [{
                UserName: {
                    $regex: new RegExp("^" + req.body.UserName, "i")
                }
            },
            {
                Email: {
                    $regex: new RegExp("^" + req.body.Email, "i")
                }
            }
        ],
        IsActive: req.body.IsActive
    }, function (err, user) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
            //res.send(err);
        }
        if (!user) {
            res.json({
                IsSuccess: false,
                Message: 'Username or Email Id does not exists',
                Data: null
            });
        } else {

            if (user.IsEmailActivated) {
                res.json({
                    IsSuccess: true,
                    Message: 'success',
                    Data: user
                });
            } else {
                res.json({
                    IsSuccess: false,
                    Message: 'Account not verified',
                    Data: user
                });
            }

        }

        // return all users in JSON format
    });
};

function getUserByRoleName(req, res) {

    RoleMaster.findOne({
        RoleName: req.body.RoleName,
        IsActive: true
    }, function (err, role) {
        if (err) {
            res.send(err);
        }
        UserMaster.find({
            _roleId: role._id,
            IsActive: true
        }, function (err, user) {

            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err) {
                res.send(err);
            } else {
                res.json({
                    IsSuccess: true,
                    Message: 'success',
                    Data: user
                });
            }
        });
    });
};


function getUserbyUserId(req, res) {
    console.log(req.body._userId);
    UserMaster.findOne({
        _id: req.body._userId,
        IsActive: true
    }, function (err, user) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
            //res.send(err);
        }
        if (!user) {
            res.json({
                IsSuccess: false,
                Message: 'Username or Email Id does not exists',
                Data: null
            });
        } else {
            res.json({
                IsSuccess: true,
                Message: 'success',
                Data: user
            });
        }
    });
};



function getRolesDetails(req, res) {
    RoleMaster.find({
        IsActive: true
    }).exec(function (err, roles) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.send(err);
        }

        res.json(roles); // return all projects in JSON format
    });
};



    // api ---------------------------------------------------------------------
    // get all users
    router.get('/api/getAllUsers', function (req, res) {
        // use mongoose to get all users in the database        
        getAllUsers(res);
    });
    router.post('/api/sendMail', function (req, res) {
        console.log("Entering into Mail send options");
        console.log(req.ResetData);
        var mailOptions = {
            to: req.body.to,
            subject: req.body.Subject,
            html: req.body.MailBody
        }
        console.log(mailOptions);
        smtpTransport.sendMail(mailOptions, function (error, response) {
            if (error) {
                console.log(error);
                res.end("error");
            } else {
                console.log("Message sent: " + response.message);
                res.end("sent");
            }
        });
    });

    // get a user
    router.post('/api/getUser', function (req, res) {        
       getUser(req, res);       
    });

    router.post('/api/getUserByEmail', function (req, res) {
        // use mongoose to get a user in the database        
        getUserbyEmail(req, res);        
    });
    
    router.get('/api/getProjectRoles/:_id', function (req, res) {
        console.log("Entered");
        UserProjectRoleMaster.find({
            IsActive: true,
            _userId: req.params._id
        }).populate('_roleId').populate('_projectId').populate('_userId').exec(function (err, projectRoleDetails) {

            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err) {
                res.json({
                    IsSuccess: false,
                    Message: err,
                    Data: err
                });
                //res.send(err);.populate('_regionDetailId').populate('_cloudEnvironmentId')
            } else {
                console.log(projectRoleDetails);
                res.json({
                    IsSuccess: true,
                    Message: "Success",
                    Data: projectRoleDetails
                });

            }

            // return all users in JSON format
        });
    });


    router.post('/api/getProjectRolesById', function (req, res) {
        console.log(req);
        UserProjectRoleMaster.find({
            IsActive: true,
            _projectId: req.body.projectId,
            _userId: req.body.userId
        }).populate('_roleId').populate('_projectId').populate('_userId').exec(function (err, projectRoleDetails) {

            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err) {
                res.json({
                    IsSuccess: false,
                    Message: err,
                    Data: err
                });
                //res.send(err);.populate('_regionDetailId').populate('_cloudEnvironmentId')
            } else {
                console.log(projectRoleDetails);
                res.json({
                    IsSuccess: true,
                    Message: "Success",
                    Data: projectRoleDetails
                });

            }

            // return all users in JSON format
        });
    });

    router.get('/api/getProjectManagerByProject/:_id', function (req, res) {
        console.log("Entered");
        UserProjectRoleMaster.find({
            IsActive: true,
            _projectId: req.params._id,
        IsPrimaryManager: true
        }).populate('_roleId').populate('_projectId').populate('_userId').exec(function (err, projectRoleDetails) {

            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err) {
                res.json({
                    IsSuccess: false,
                    Message: err,
                    Data: err
                });
                //res.send(err);.populate('_regionDetailId').populate('_cloudEnvironmentId')
            } else {
                console.log(projectRoleDetails);
                res.json({
                    IsSuccess: true,
                    Message: "Success",
                    Data: projectRoleDetails
                });

            }

            // return all users in JSON format
        });
    });
    router.get('/api/getProjectRolesByProject/:_id', function (req, res) {
        console.log("Entered");
        UserProjectRoleMaster.find({
            IsActive: true,
            _projectId: req.params._id
        }).populate('_roleId').populate('_projectId').populate('_userId').exec(function (err, projectRoleDetails) {

            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err) {
                res.json({
                    IsSuccess: false,
                    Message: err,
                    Data: err
                });
                //res.send(err);.populate('_regionDetailId').populate('_cloudEnvironmentId')
            } else {
                console.log(projectRoleDetails);
                res.json({
                    IsSuccess: true,
                    Message: "Success",
                    Data: projectRoleDetails
                });

            }

            // return all users in JSON format
        });
    });
        

    // create users and send back all users after creation
    router.post('/api/addUsers', function (req, res) {
        var url = req.protocol + '://' + req.get('host');
        var ObjectID = mongoose.Types.ObjectId;
		var id = new ObjectID();
        var imgPath = path.dirname('images') + '//images//profilepics//' + req.body.UserName + '_' + id + '.jpg';
        
        UserMaster.find({
            Email: req.body.Email,
            IsActive: true
        }, function (err, user) {

            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err) {
                res.json({
                    IsSuccess: false,
                    Message: err,
                    Data: err
                });
                //res.send(err);
            }
            if (user.length > 0) {
                res.json({
                    IsSuccess: false,
                    Message: 'Username/Email already exists',
                    Data: null
                });
            } else {

            if (req.body.userimageBytes && req.body.userimageBytes.base64) {
                    var bitmap = new Buffer(req.body.userimageBytes.base64, 'base64');
                    fs.writeFileSync(imgPath, bitmap);
                    req.body.userimageBytes = ""; //req.body.userimageBytes.base64;
                    req.body.userimageDirPath = url + '//images//profilepics//' + req.body.UserName + '_' + id + '.jpg';
                    req.body.userimageName = req.body.UserName + '_' + id + '.jpg';
                }

                UserMaster.create(req.body, function (err, post) {
                    if (err) {
                        res.json({
                            IsSuccess: false,
                            Message: err,
                            Data: err
                        });
                    } else {
                        res.json({
                            IsSuccess: true,
                            Message: 'New User has been added successfully',
                            data: post
                        });
                    }
                });
            }

            // return all users in JSON format
        });
    });
   
    // create a user, information comes from AJAX request from Angular



    router.put('/api/updateUserDetails/:_id', function (req, res) {       
        
        var url = req.protocol + '://' + req.get('host');
        var ObjectID = mongoose.Types.ObjectId;
		var id = new ObjectID();
        var imgPath = path.dirname('images') + '//images//profilepics//' + req.body.UserDetails.UserName + '_' + id + '.jpg';
        
    if (req.body.FileBytes && req.body.FileBytes.base64) {
            //console.log(req.body.FileBytes)
            var bitmap = new Buffer(req.body.FileBytes.base64, 'base64');
            fs.writeFileSync(imgPath, bitmap);
            req.body.UserDetails.userimageBytes = ""; //req.body.FileBytes.base64;
            req.body.UserDetails.userimageDirPath = url + '//images//profilepics//' + req.body.UserDetails.UserName + '_' + id + '.jpg';
            req.body.UserDetails.userimageName = req.body.UserDetails.UserName + '_' + id + '.jpg';
        }

        UserMaster.update({
            _id: req.params._id
        }, {
            $set: {
            IsActive: req.body.UserDetails.IsActive,
            IsEmailActivated: req.body.UserDetails.IsEmailActivated,
            IsRequestedforResetPwd: req.body.UserDetails.IsRequestedforResetPwd,
            IsRequestedforResetPwdTime: req.body.UserDetails.IsRequestedforResetPwdTime,
            IsSuperAdmin: req.body.UserDetails.IsSuperAdmin,
            IsPrimaryManager: req.body.UserDetails.IsPrimaryManager,
            _roleId: req.body.UserDetails._roleId,
            ModifiedDate: req.body.UserDetails.ModifiedDate,
            UserName: req.body.UserDetails.UserName,
            Email: req.body.UserDetails.Email,
            Password: req.body.UserDetails.Password,
            FirstName: req.body.UserDetails.FirstName,
            LastName: req.body.UserDetails.LastName,
                LastLoginTime: req.body.UserDetails.LastLoginTime,
            Title: req.body.UserDetails.Title,
            Mobile: req.body.UserDetails.Mobile,
            userimageName: req.body.UserDetails.userimageName,
            userimageBytes: req.body.UserDetails.userimageBytes,
            userimageDirPath: req.body.UserDetails.userimageDirPath,
            imagetype: req.body.UserDetails.imagetype,
            CreatedBy: req.body.UserDetails.CreatedBy,
            ModifiedBy: req.body.UserDetails.ModifiedBy,
                userOriginalImgName: req.body.UserDetails.userOriginalImgName,
            }
        }, function (err, result) {
            if (err) {
                res.json({
                    IsSuccess: false,
                    Message: err,
                    Data: err
                });
            } else {
                res.json({
                    IsSuccess: true,
                    Message: "User details has been updated successfully",
                    Data: result
                });
            }
        })

    });
    // delete a user
    router.delete('/api/deleteUser/:_id', function (req, res) {
        console.log(req.params._id);
        UserMaster.update({
            _id: req.params._id
        }, {
            $set: {
                IsActive: false
            }
        }, function (err, result) {
            if (err)
                res.send(err);
            else
                res.json({
                    IsSuccess: true,
                    Message: "User has been deleted successfully"
                });
        });
});
router.get('/api/updateSuperUsers', function (req, res) {



    UserMaster.find({
        _roleId: 10

    }, function (err, user) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
            //res.send(err);
        }
        else {
            if (user.length > 0) {
                console.log(user);
                user.forEach(function (assignedUserDetails) {
                    console.log(assignedUserDetails);
                    UserMaster.update({
                        _id: assignedUserDetails._id
                    }, {
                            $set: {
                                _roleId: 0,
                            }
                        }, function (err, result) {
                            if (err) {
                                res.json({
                                    IsSuccess: false,
                                    Message: err,
                                    Data: err
                                });
                            } else {
                                // res.json({
                                //     IsSuccess: true,
                                //     Message: "User details has been updated successfully",
                                //     Data: result
                                // });
                            }
                        })
        });
        res.json({
            IsSuccess: true,
            Message: "User details has been updated successfully",
            Data: null
        });
            }
            else{
                res.json({
                    IsSuccess: true,
                    Message: "User details has been updated successfully",
                    Data: user
    });
            }
            

        }


    });
})
    // create super users
router.post('/api/saveSuperUsers', function (req, res) {    
       req.body.forEach(function (data) {        
        UserMaster.findOne({
            UserName: data.UserName,
            //Email: data.Email,
            IsActive: true
        }, function (err, user) {    
            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err) {                
                res.json({
                    IsSuccess: false,
                    Message: err,
                    Data: err
                });                
            }
            
            if (user != null) {                 
                UserMaster.update({
                    _id: user._id
                }, {
                       $set: {
                        Email: data.Email,
                        _roleId : data._roleId             
                    }
                    }, function (err, result) {
                        if (err)
                            res.send(err);  
                    });
            }
            else {                           
                UserMaster.create(data, function (err, post) {
                    if (err) {
                        
                        res.json({
                            IsSuccess: false,
                            Message: err,
                            Data: err
                        });
                    } 
                });
            }
    
        });
    });   
    
    res.json({
        IsSuccess: true,
        Message: 'Super user has been added successfully'
    });

});

    
    // get all Roles
    router.get('/api/getRolesDetails', function (req, res) {
        // use mongoose to get all projects in the database

        getRolesDetails(req, res);
    });

    // get Roles by Id
    router.get('/api/getRolesDetailById/:_id', function (req, res) {
        RoleMaster.findOne({
            IsActive: true,
            _id: req.params._id
        }, function (err, result) {
    
            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err) {
                res.json({
                    IsSuccess: false,
                    Message: err,
                    Data: err
                });
                
            } else {
                res.json({
                    IsSuccess: true,
                    Message: "Success",
                    Data: result
                });
            }    
        });
    });

    router.put('/api/updateRoleDetailStatus/:_id', function (req, res) {
        console.log(req.params._id);
        RoleMaster.update({
            _id: req.params._id
        }, {
            $set: {
                IsRoleStatus: req.body.IsRoleStatus,               
            }
        }, function (err, post) {
            if (err)
                res.send(err);
            else
            RoleMaster.findOne({
                _id: req.params._id,
                IsActive: true
            }, function (err, result) {
                if (err) {
                    res.json({
                        IsSuccess: false,
                        Message: err,
                        Data: err
                    });
                }
                else {
                    res.json({
                        IsSuccess: true,
                        Message: "Role Status has been updated successfully",
                        Data: result
                    });
                }
            });
        });
    });

    router.put('/api/updateRoleDetails/:_id', function (req, res, next) {    

        RoleMaster.findOne({
            _id: req.params._id,
            IsActive: true
        }, function (err, result) {
            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err) {
                res.json({
                    IsSuccess: false,
                    Message: err,
                    Data: err
                });
                
            } else {
                RoleMaster.update({
                    _id: req.params._id
               }, 
               {
                   $set: req.body
                }, function (err, post) {
                   if (!result) {
                       res.json({
                           IsSuccess: false,
                           Message: err
                       });
                   } else {
                       res.json({
                           IsSuccess: true,
                           Message: "Role has been updated successfully",
                           Data: post
                       });
                   }
               });
            }
        });
        
    });
                    
    

    // get user by id
    router.post('/api/getUserByUserId', function (req, res) {
        // use mongoose to get a user in the database        
        getUserbyUserId(req, res);
    });
    
    // get user by id
    router.post('/api/getUserByRoleName', function (req, res) {
        // use mongoose to get a user in the database        
        getUserByRoleName(req, res);
    });
  
        
    
    router.post('/upload', function (req, res) {
        console.log(req.files);
        res.json({
            success: true
        });
        //res.sendFile(path.join(__dirname, '../router/views/Admin', 'AdministratorDashboard.html'));
        //res.sendFile(path.join(__dirname + 'router/Admin/views/AdministratorDashboard.html'));//res.sendFile('/Test.html'); // load the single view file (angular will handle the page changes on the front-end)
    });



module.exports = router;