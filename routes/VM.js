var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var path = require('path');
var http = require('http');
var deleteProject = require("./InstanceCRUD");
var manager = new deleteProject();
var EnvironmentMaster = require('../models/EnvironmentConfigureSchema.js');
var VMProjectEnvironmentConfigurationMaster = require('../models/VMProjectEnvironmentConfigurationSchema.js');
var RegionDetailMaster = require('../models/RegionDetailSchema.js');
var VMTemplateDetailMaster = require('../models/VMTemplateDetailSchema.js');
var CloudEnvironmentMaster = require('../models/CloudEnvironmentSchema.js');
var VMTemplateMaster = require('../models/VMTemplateDetailSchema.js');
var InstanceTypeDetailMaster = require('../models/InstanceTypeDetailSchema.js');

function updateInstanceStatusById(req, res, next) {
    var obj = req.body;
    obj.InstanceStatus = req.Status;
    VMProjectEnvironmentConfigurationMaster.update({
        InstanceId: req.body.InstanceId.toString()
    }, {
            $set: obj
        }).exec().then(function (result) {
            if (err) {
                return next(err);
            } else {
                console.log(
                    "Instance status has been updated successfully"
                );
            }
        })
}

function getProjectVMAssignments(req, res, next) {
    VMProjectEnvironmentConfigurationMaster.find({
        IsActive: true
    }).populate('_projectId').populate('_cloudEnvironmentId').populate('_VMTemplateDetailId').populate('_environmentConfigureId').exec(function (err, projectVM) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            return next(err);
        }

        res.json(projectVM); // return all projectVM in JSON format
    });
};


// get all projects VM Assignments
router.get('/api/getProjectVMAssignments', function (req, res, next) {
    // use mongoose to get all projects in the database

    getProjectVMAssignments(req, res, next);
});

//Creating a VM
router.post('/api/createVMwithDetails', function (req, res, next) {
    console.log("Entered createVMwithDetails Method");

    // Load the AWS SDK for Node.js
    var AWS = require('aws-sdk');
    // Set the region 
    //AWS.config.update({region: 'us-west-2'});

    //Configuring AWS Environment
    //AWS.config.loadFromPath('../Cloud Flex Development/config.json');


    var configFile = require('../config.json');

    AWS.config.update({
        accessKeyId: configFile.accessKeyId, //"AKIAITAAUVFATOV3ICIA"
        secretAccessKey: configFile.secretAccessKey, //"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"
        "region": req.body.Region // "ap-southeast-1"  
    });

    // Create EC2 service object
    var ec2 = new AWS.EC2({
        apiVersion: '2016-11-15'
    });

    var params = {
        ImageId: req.body.ImageID, // amzn-ami-2011.09.1.x86_64-ebs
        InstanceType: req.body.InstanceType, //'t2.micro',
        MinCount: 1,
        MaxCount: 1,
        KeyName: configFile.KeyName //"valholla-admin"
    };

    var projectName = req.body.ProjectName + " " + req.body.Environment;

    // Create the instance
    ec2.runInstances(params, function (err, data) {
        if (err) {
            console.log("Could not create instance", err);
            return next(err);
        }
        var instanceId = data.Instances[0].InstanceId;
        var instancedetails = data.Instances[0];
        console.log("Created instance", instanceId);
        // Add tags to the instance
        params = {
            Resources: [instanceId],
            Tags: [{
                Key: 'Name',
                Value: projectName
            }]
        };
        ec2.createTags(params, function (err) {
            console.log("Tagging instance", err ? "failure" : "success");
            if (req.body.IsNewTemplate) {
                var objVM = {
                    TemplateName: req.body.TemplateName,
                    TemplateDescription: req.body.TemplateDescription,
                    _cloudEnvironmentId: req.body.CloudEnvironmentId,
                    _instanceTypeId: req.body.InstanceTypeId,
                    _regionDetailId: req.body.RegionDetailId,
                    _OSId: req.body.OSId,
                    VMStatus: null,
                    IsActive: true,
                    IsSavedTemplate: req.body.IsTempSaveorNot,
                    IsDirectSavedTemplate: false,
                    CreatedBy: req.body.CreatedBy,
                    ModifiedBy: null,
                    CreatedDate: req.body.CreatedDate,
                    ModifiedDate: null
                };
                VMTemplateDetailMaster.create(objVM, function (err, result) {
                    if (err) {
                        return next(err);
                    } else {
                        var objAssign = {
                            _projectId: req.body.ProjectId,
                            _cloudEnvironmentId: req.body.CloudEnvironmentId,
                            _VMTemplateDetailId: result._id,
                            _environmentConfigureId: req.body.EnvironmentId,
                            InstanceId: instanceId,
                            InstanceStatus: 'running',
                            IsActive: true,
                            CreatedBy: req.body.CreatedBy,
                            ModifiedBy: null,
                            CreatedDate: req.body.CreatedDate,
                            ModifiedDate: null
                        };
                        VMProjectEnvironmentConfigurationMaster.create(objAssign, function (err, post) {
                            if (err) {
                                return next(err);
                            } else {
                                res.json({
                                    IsSuccess: true,
                                    Message: ("New Instance has been created successfully in " + req.body.Environment + " cloud environment with Instance ID -  " + instanceId),
                                    Data: instancedetails
                                });
                            }
                        });
                    }
                });


            } else {
                var objAssign = {
                    _projectId: req.body.ProjectId,
                    _cloudEnvironmentId: req.body.CloudEnvironmentId,
                    _VMTemplateDetailId: req.body.VMTemplateDetailId,
                    _environmentConfigureId: req.body.EnvironmentId,
                    InstanceId: instanceId,
                    InstanceStatus: 'running',
                    IsActive: true,
                    CreatedBy: req.body.CreatedBy,
                    ModifiedBy: null,
                    CreatedDate: req.body.CreatedDate,
                    ModifiedDate: null
                };
                VMProjectEnvironmentConfigurationMaster.create(objAssign, function (err, post) {
                    if (err) {
                        return next(err);
                    } else {
                        res.json({
                            IsSuccess: true,
                            Message: ("Instance has been created successfully in AWS cloud environment with Instance ID - " + instanceId),
                            Data: instancedetails
                        });
                    }
                });
            }

        });
    });
});


router.post('/api/updateInstanceStatus', function (req, res, next) {
    RegionDetailMaster.findOne({
        _id: req.body.RegionId,
        IsActive: true
    }, function (err, region) {
        //console.log(region)
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
            //res.send(err);
        } else {



            var AWS = require('aws-sdk');
            var configFile = require('../config.json');

            AWS.config.update({
                accessKeyId: configFile.accessKeyId,
                secretAccessKey: configFile.secretAccessKey,
                region: region.RegionCode
            });
            // AWS.config.loadFromPath('../Cloud Flex Development/config.json');
            var ec2 = new AWS.EC2({
                apiVersion: '2016-11-15'
            });
            // AWS.config.update({region: region.RegionCode}); 
            var params = {
                InstanceIds: [ /* required */
                    req.body.InstanceId,
                    /* more items */
                ],
                //AdditionalInfo: 'STRING_VALUE',
                //DryRun: true || false
            };
            switch (req.body.status) {
                case "15":
                    //start instances
                    ec2.startInstances(params, function (err, data) {
                        if (err) console.log(err, err.stack); // an error occurred
                        else {
                            req.Status = "running";
                            //console.log(data);  
                            updateInstanceStatusById(req)
                            res.json({
                                IsSuccess: true,
                                Message: data
                            });

                        } // successful response

                    });
                    break;
                case "80":
                    //Stop instancecs
                    ec2.stopInstances(params, function (err, data) {
                        if (err) console.log(err, err.stack); // an error occurred
                        else {
                            req.Status = "stopped";
                            //console.log(data);  
                            updateInstanceStatusById(req)
                            res.json({
                                IsSuccess: true,
                                Message: data
                            });

                        } // successful response

                    });
                    break;
                case "16":
                    // reboot instances
                    ec2.rebootInstances(params, function (err, data) {
                        if (err) console.log(err, err.stack); // an error occurred
                        else {
                            req.Status = "running";
                            // console.log(data);  
                            updateInstanceStatusById(req)
                            res.json({
                                IsSuccess: true,
                                Message: data
                            });

                        } // successful response

                    });
                    break;
                case "48":
                    // terminate Instance
                    ec2.terminateInstances(params, function (err, data) {
                        if (err) console.log(err, err.stack); // an error occurred
                        else {
                            req.Status = "terminated";
                            //console.log(data);  
                            updateInstanceStatusById(req)
                            res.json({
                                IsSuccess: true,
                                Message: data
                            });

                        } // successful response

                    });

                    break;
                default:
                    ec2.rebootInstances(params, function (err, data) {
                        if (err) console.log(err, err.stack); // an error occurred
                        else {
                            // console.log(data);  
                            updateInstanceStatusById(req)
                            res.json({
                                IsSuccess: true,
                                Message: data
                            });

                        } // successful response

                    });
                    break;
            }


        }
    });

    // use mongoose to get all projects in the database


});


router.post('/api/changeInstanceStatus', function (req, res, next) {
    console.log(req.body.status);
    VMProjectEnvironmentConfigurationMaster.update({
        InstanceId: req.body.InstanceId
    }, {
        $set: {
            InstanceStatus: req.body.status
        }
        }, function (err, numberAffected, rawResponse) {
            if (err) {
                return next(err);
            } else {
                res.json({
                    IsSuccess: true,
                    Message: "Instance status has been updated successfully"
                });
            }
        })
});


// check project assigning to VM template
router.post('/api/checkProjectAssigntoVM', function (req, res, next) {
    VMProjectEnvironmentConfigurationMaster.find({
        _projectId: req.body._projectId,
        InstanceStatus: {
            $ne: "terminated"
        },
        IsActive: true
    }, function (err, projects) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
            //res.send(err);
        }

        if (projects.length > 0) {
            res.json({
                IsSuccess: true,
                Message: 'success',
                Data: projects
            });

        } else {
            res.json({
                IsSuccess: false,
                Message: 'Project does not exists',
                Data: null
            });
        }
    });
});


router.post('/api/savetemplateDetails', function (req, res) {

    VMTemplateDetailMaster.create(req.body, function (err, post) {
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
        } else {
            res.json({
                IsSuccess: true,
                Message: 'New VM template has been added successfully',
                data: post
            });
        }
    });


});


router.put('/api/updateTemplateDetails/:template_id', function (req, res) {

    VMTemplateDetailMaster.update({
        _id: req.params.template_id
    }, {
            TemplateName: req.body.TemplateDetails.TemplateName,
            TemplateDescription: req.body.TemplateDetails.TemplateDescription,
            _cloudEnvironmentId: req.body.TemplateDetails._cloudEnvironmentId,
            _instanceTypeId: req.body.TemplateDetails._instanceTypeId,
            _regionDetailId: req.body.TemplateDetails._regionDetailId,
            _OSId: req.body.TemplateDetails._OSId,
            IsActive: req.body.TemplateDetails.IsActive,
            ModifiedBy: req.body.TemplateDetails.ModifiedBy,
            ModifiedDate: req.body.TemplateDetails.ModifiedDate,
            IsSavedTemplate: req.body.TemplateDetails.IsSavedTemplate,
            IsDirectSavedTemplate: req.body.TemplateDetails.IsDirectSavedTemplate,
            VMStatus: req.body.TemplateDetails.VMStatus,

        }, req.body, function (err, numberAffected, rawResponse) {
            if (err) {
                res.json({
                    IsSuccess: false,
                    Message: err,
                    Data: err
                });
            } else {
                res.json({
                    IsSuccess: true,
                    Message: "VM template has been updated successfully",
                    Data: err
                });
            }
        })


});


router.get('/api/getTemplates', function (req, res) {
    VMTemplateMaster.find({
        IsActive: true,
        IsSavedTemplate: true
    }).populate('_instanceTypeId').populate('_regionDetailId').populate('_cloudEnvironmentId').exec(function (err, templates) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
            //res.send(err);.populate('_regionDetailId').populate('_cloudEnvironmentId')
        } else {
            res.json(templates);

        }

        // return all users in JSON format
    });
});

router.get('/api/getTemplatesById/:_id', function (req, res) {
    VMTemplateMaster.findOne({
        IsActive: true,
        IsSavedTemplate: true,
        _id: req.params._id
    }).populate('_instanceTypeId').populate('_regionDetailId').populate('_cloudEnvironmentId').exec(function (err, templates) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
            //res.send(err);.populate('_regionDetailId').populate('_cloudEnvironmentId')
        } else {
            res.json(templates);

        }

        // return all users in JSON format
    });
});

router.get('/api/getTemplatesForVMAssignById/:_id', function (req, res) {
    VMTemplateMaster.findOne({
        _id: req.params._id
    }).populate('_instanceTypeId').populate('_regionDetailId').populate('_cloudEnvironmentId').exec(function (err, templates) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
            //res.send(err);.populate('_regionDetailId').populate('_cloudEnvironmentId')
        } else {
            res.json(templates);

        }

        // return all users in JSON format
    });
});

router.delete('/api/deleteTemplateById/:_id', function (req, res) {
    VMTemplateMaster.update({
        _id: req.params._id
    }, {
            $set: {
                IsActive: false
            }
        }, function (err, result) {
            if (err)
                res.send(err);
            else{
               
                VMProjectEnvironmentConfigurationMaster.find({
                    IsActive: true,
                    _VMTemplateDetailId: req.params._id
                }).populate('_projectId').populate('_cloudEnvironmentId').populate('_VMTemplateDetailId').populate('_environmentConfigureId').exec(function (err, projectVM) {
                    console.log("2");
                    // if there is an error retrieving, send the error. nothing after res.send(err) will execute
                    if (err) {
                        res.json({
                            IsSuccess: false,
                            Message: err,
                            Data: err
                        });
                    }
                    else {
                        manager.getRegionDetails(projectVM,req,res);
                        // res.json(projectVM); // return all projectVM in JSON format
    
                    }
    
                });
            }
            
          
        });
});

module.exports = router;