﻿var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var path = require('path');
var http = require('http');
var IssueTrackingMaster=require('../models/issueTrackingSchema.js');
var NotificationAapprovalMaster=require('../models/NotificationApprovalSchema.js');
var EnvironmentMaster = require('../models/EnvironmentConfigureSchema.js');
var BuildMaster = require('../models/BuildSchema.js');
var BuildPhaseMaster = require('../models/BuildPhaseSchema.js');
var CloudEnvironmentMaster = require('../models/CloudEnvironmentSchema.js');
var RegionDetailMaster = require('../models/RegionDetailSchema.js');
var InstanceTypeDetailMaster = require('../models/InstanceTypeDetailSchema.js');
var OSDetailMaster = require('../models/OSDetailSchema.js');
var BuildConfigurationsSchemaMaster = require('../models/BuildConfigurationsSchema.js');
var NotificationSchemaMaster = require('../models/NotificationSchema.js');
var ActiveDirectory = require('activedirectory');
var VMProjectEnvironmentConfigurationMaster = require('../models/VMProjectEnvironmentConfigurationSchema.js');
var configFile = require('../config.json');

//List all builds
function getBuildDetails(req, res, next) {
    BuildConfigurationsSchemaMaster.find({
        IsActive: true
    }).populate('_projectId').populate('_repositoryId').exec(function (err, builds) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            return next(err);
        }

        res.json(builds); // return all builds in JSON format
    });
};
function getBuildDetailsByProjectID(req, res, next) {
    BuildConfigurationsSchemaMaster.find({
        IsActive: true,
        _projectId: req.params._id
    }).populate('_projectId').populate('_repositoryId').exec(function (err, builds) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            return next(err);
        }

        res.json(builds); // return all builds in JSON format
    });
};
function getAllBuildDetailsByProjectID(req, res, next) {
    BuildConfigurationsSchemaMaster.find({        
        _projectId: req.params._id
    }).populate('_projectId').populate('_repositoryId').exec(function (err, builds) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            return next(err);
        }

        res.json(builds); // return all builds in JSON format
    });
};
function getBuildDetailsByBuildID(req, res, next) {
    BuildConfigurationsSchemaMaster.findOne({
        _id: req.params._id,
        IsActive: true
    }).populate('_projectId').populate('_repositoryId').exec(function (err, builds) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
        }

        res.json({
            IsSuccess: true,
            Message: "Success",
            Data: builds
        }); // return all builds in JSON format
    });
};

// get all builds
router.get('/api/getBuildDetails', function (req, res, next) {
    // use mongoose to get all Build from the database

    getBuildDetails(req, res, next);
});
router.get('/api/getBuildDetailsByProjectID/:_id', function (req, res, next) {
    // use mongoose to get all Build from the database

    getBuildDetailsByProjectID(req, res, next);
});

router.get('/api/getAllBuildDetailsByProjectID/:_id', function (req, res, next) {
    // use mongoose to get all Build from the database

    getAllBuildDetailsByProjectID(req, res, next);
});

router.get('/api/getBuildDetailsByBuildID/:_id', function (req, res, next) {
    // use mongoose to get all Build from the database

    getBuildDetailsByBuildID(req, res, next);
});
router.get('/api/getBuildDetailById/:_id', function (req, res, next) {
    console.log(req.params._id);
    BuildMaster.findOne({
        IsActive: true,
        _id: req.params._id
    }).populate('_buildId').exec(function (err, result) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
        } else {

            BuildPhaseMaster.find({
                IsActive: true,
                _buildId: req.params._id
            }, function (err, post) {
                if (err) {
                    res.json({
                        IsSuccess: false,
                        Message: err,
                        Data: err
                    });
                } else {
                    var obj = {
                        _id: result._id,
                        _projectId: result._projectId,
                        _environmentConfigureId: result._environmentConfigureId,
                        _buildConfigurationId: result._buildConfigurationId,
                        BuildVersion: result.BuildVersion,
                        BuildPhases: post,
                        BuildRequisition: result.BuildRequisition,
                        BuildVersionType: result.BuildVersionType,
                        DeploymentUrl: result.DeploymentUrl,
                        InstanceId: result.InstanceId,
                        IsActive: result.IsActive,
                        CreatedDate: result.CreatedDate,
                        ModifiedDate: result.ModifiedDate,
                        CreatedBy: result.CreatedBy,
                        ModifiedBy: result.ModifiedBy,
                    }
                    res.json({
                        IsSuccess: true,
                        Message: "Success",
                        Data: obj
                    });
                }

            });

        }

    });
});

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

router.post('/api/getUserDetails', function (req, res, next) {
        
    var config = {
        url: req.body.userDetails.ADConfig.url, //ldap://GMI-DC.GlobalMantrai.com
        baseDN: req.body.userDetails.ADConfig.baseDN, //dc=GlobalMantrai,dc=com
        //username: 'valhollatesting\\Administrator',
        //password: 'rnjpGll!2ih(5sK3KzhmV43trd$w?T!N',                
    }
    var ad = new ActiveDirectory(config);

	var domainName = req.body.userDetails.ADConfig.domainName;
	var userNamewithDomain = req.body.userDetails.userName;
	
	//Check if username or Email
	if(!validateEmail(userNamewithDomain))
	{
		userNamewithDomain = domainName + "\\" + userNamewithDomain;
	}
		
    ad.authenticate(userNamewithDomain, req.body.userDetails.password, function (err, auth) {
        //console.log(err)
        if (err) {
            console.log('ERROR: ');
            res.json({
                IsAuthenticate: false,
                Message: "Authentication failed",
                Data: null
            });
            return;
        }

        if (auth) {
            console.log(auth + ' Authenticated!');
            var opts = {
                bindDN: req.body.userDetails.ADConfig.bindDN,//"valhollatesting\\Administrator",
                bindCredentials: req.body.userDetails.ADConfig.bindCredentials
            };
            ad.findUser(opts, req.body.userDetails.userName, function (err, user) {
                if (err) {
                    res.json({
                        IsAuthenticate: true,
                        IsSuccess: false,
                        Message: 'User: ' + req.body.userDetails.userName + ' not found.',
                        Data: null
                    });
                  //  console.log('ERROR: ' + JSON.stringify(err));
                    return;
                }

                if (!user) {
                    console.log('User: ' + req.body.userDetails.userName + ' not found.'); res.json({IsAuthenticate: true,
                        IsSuccess: false,
                        Message: 'User: ' + req.body.userDetails.userName + ' not found.',
                        Data: null
                    });
                }
                else {
                    if(user.sAMAccountName!=null || user.sAMAccountName!=undefined){
                        ad.getGroupMembershipForUser(opts, user.sAMAccountName, function (err, groups) {
                            if (err) {
                                console.log('ERROR: ' + JSON.stringify(err));
                                return;
                            }
            
                            if (!groups){
                                console.log('User: ' + user.sAMAccountName + ' not found.');
                            } 
                            else{
                                console.log(JSON.stringify(groups));
                               // console.log(JSON.stringify(groups.cn));
                                
                                res.json({
                                    IsAuthenticate: true,
                                    IsSuccess: true,
                                    Message: "Success",
                                    Data: user,
                                    Group:groups
                                });
                            }
    
                        });
                    }

                   
                        console.log(JSON.stringify(user));
                }
            });
            //   ad.getUsersForGroup(opts,'User', function(err, group) {
            //       console.log("Entering")
            //     if (err) {
            //       console.log('ERROR: ' +JSON.stringify(err));
            //       return;
            //     }

            //     if (!group) console.log('Group: SuperUser not found.');
            //     else {
            //       console.log(group);
            //       console.log('Members: ' + (group.member || []).length);
            //     }
            //   });
            
        }
        else {
            res.json({
                IsAuthenticate: true,
                IsSuccess: false,
                Message: 'Authentication failed',
                Data: null
            });
        }
    });

});

router.post('/api/getUserDetailByADGroupname', function (req, res, next) {
    //console.log(req.body);    
    var config = {
        url: req.body.url, //ldap://GMI-DC.GlobalMantrai.com
        baseDN: req.body.baseDN, //dc=GlobalMantrai,dc=com
        username: req.body.bindDN,
        password: req.body.bindCredentials,                
    }
    var ad = new ActiveDirectory(config);

    var opts = {
        bindDN: req.body.bindDN,//"valhollatesting\\Administrator",
        bindCredentials: req.body.bindCredentials
    };   
    ad.getUsersForGroup(req.body.groupName, function(err, users) {
       
        if (err) {
            console.log('ERROR: ' + JSON.stringify(err));
            res.json({
                IsSuccess: false,
                Message: err,
                Data: null
            });
        }

        if (!users){            
            res.json({
                IsSuccess: false,
                Message: 'Group: ' + req.body.groupName + ' not found.',
                Data: null
            });
        } 
        else{    
            
            res.json({
                IsAuthenticate: true,
                IsSuccess: true,
                Message: "Success",
                Data: users
            });
        }

    });

});

router.post('/api/getUserDetailByUsername', function (req, res, next) {      
    var config = {
        url: req.body.url, //ldap://GMI-DC.GlobalMantrai.com
        baseDN: req.body.baseDN, //dc=GlobalMantrai,dc=com
        //username: 'valhollatesting\\Administrator',
        //password: 'rnjpGll!2ih(5sK3KzhmV43trd$w?T!N',                
    }
    var ad = new ActiveDirectory(config);
    var opts = {
        bindDN: req.body.bindDN,//"valhollatesting\\Administrator",
        bindCredentials: req.body.bindCredentials
    };
    ad.findUser(opts, req.body.userName, function (err, user) {
        if (err) {
            res.json({                
                IsSuccess: false,
                Message: 'User: ' + req.body.userName + ' doesn’t exist',
                Data: null
            }); 
        }

        if (!user) {
            console.log('User: ' + req.body.userName + ' doesn’t exist'); 
            res.json({
                IsSuccess: false,
                Message: 'User: ' + req.body.userName + ' doesn’t exist',
                Data: null
            });
        }
        else {            
            res.json({               
                IsSuccess: true,
                Message: "Success",
                Data: user                
            });
        }
    });  
});

router.post('/api/saveInitiateBuildDetails', function (req, res, next) {
    var buildPhase = ["GITPULL", "CODEBUILD", "UNITTEST", "SECURITYTEST", "DEPLOYMENT", "UFT", "COMPLETE"];
    var buildPhaseArray = [];

    BuildMaster.create(req.body, function (err, post) {
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err
            });
        } else {
            //console.log(req.body);
            buildPhase.forEach(function (data) {
                var objPhase = {
                    _buildId: post._id,
                    PhaseName: data,
                    PhaseDescription: data,
                    Progress: "0",
                    Status: "Queued",
                    StatusInfo: "",
                    PhaseStartTime: new Date(),
                    PhaseEndTime: new Date(),
                    IsActive: true,
                    CreatedBy: req.body.CreatedBy,
                    ModifiedBy: null,
                    CreatedDate: req.body.CreatedDate,
                    ModifiedDate: null,
                    DeploymentUrl: null
                }
                buildPhaseArray.push(objPhase);
            });
            BuildPhaseMaster.create(buildPhaseArray, function (err, result) {
                if (err) {
                    res.json({
                        IsSuccess: false,
                        Message: err
                    });
                } else {
                    var reqBuild = require('../buildengine/buildmanager.js');
                    
                    reqBuild.startBuild(post._id, req.body.CreatedBy, req.body._environmentConfigureId, req.body.isHavetoDeploy);

                    res.json({
                        IsSuccess: true,
                        Message: "Initiate build saved successfully",
                        Data: post._id
                    });
                }

            });
        }
    });
});

router.post('/api/ReInitiateBuildDetails', function (req, res, next) {
    console.log(req.body);
    var reqBuild = require('../buildengine/buildmanager.js');
    reqBuild.startBuild(req.body._buildId, req.body.CreatedBy, req.body._environmentConfigureId, req.body.isHavetoDeploy);

    res.json({
        IsSuccess: true,
        Message: "Initiate build saved successfully"        
    });
});
router.get('/api/getBuildConfigurationDetailsByProjectID/:_id', function (req, res) {
    BuildMaster.find({
        IsActive: true,
        _projectId: req.params._id
    }).populate('_buildConfigurationId').populate('_projectRepositoryId').exec(function (err, result) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
        }
        else {
            res.json({
                IsSuccess: true,
                Message: "Success",
                Data: result
            });
        }
    })
});

router.get('/api/getBuildConfigurationDetails', function (req, res) {
    BuildMaster.find({
        IsActive: true
    }).populate('_buildConfigurationId').populate('_projectRepositoryId').exec(function (err, result) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
        }
        else {
            res.json({
                IsSuccess: true,
                Message: "Success",
                Data: result
            });
        }
    })
});
function getNotificationApprovalDetailsById(req, res, next) {
    NotificationAapprovalMaster.find({
      //  _id: req.params._id,
        IsActive: true
    }).exec(function (err, issueDetails) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
        }

        res.json({
            IsSuccess: true,
            Message: "Success",
            Data: issueDetails
        }); // return all builds in JSON format
    });
};
router.get('/api/getNotificationApprovalDetailsById', function (req, res, next) {
    // use mongoose to get all Build from the database

    getNotificationApprovalDetailsById(req, res, next);
});
router.put('/api/updateNotificationApproval/:_id', function (req, res) {
    console.log(req.params._id);
    NotificationAapprovalMaster.update({
        _id: req.params._id
    }, {
        $set: {
            From: req.body.From,
            To:req.body.To,
            ServerName: req.body.ServerName,UserId:req.body.UserId,
            Password:req.body.Password,
        }
    }, function (err, post) {
        if (err)
            res.send(err);
        else
        res.json({
            IsSuccess: true,
            Message: 'Notification Setup Details has been updated successfully',
            data: post
        });
    });
});
router.post('/api/saveNotificationApproval', function (req, res) {
    console.log(req.body)
    NotificationAapprovalMaster.create(req.body, function (err, post) {
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
        } else {
            res.json({
                IsSuccess: true,
                Message: 'Notification Setup Details has been added successfully',
                data: post
            });
        }
    });
});




function getIssueTrackingDetailsById(req, res, next) {
    IssueTrackingMaster.find({
      //  _id: req.params._id,
        IsActive: true
    }).exec(function (err, issueDetails) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
        }

        res.json({
            IsSuccess: true,
            Message: "Success",
            Data: issueDetails
        }); // return all builds in JSON format
    });
};
router.get('/api/getIssueTrackingDetailsById', function (req, res, next) {
    // use mongoose to get all Build from the database

    getIssueTrackingDetailsById(req, res, next);
});
router.put('/api/updateIssueTracking/:_id', function (req, res) {
    console.log(req.params._id);
    IssueTrackingMaster.update({
        _id: req.params._id
    }, {
        $set: {
            Url: req.body.Url,
            UrlPort:req.body.UrlPort,
            UserName: req.body.UserName,
            Password:req.body.Password,
        }
    }, function (err, post) {
        if (err)
            res.send(err);
        else
        res.json({
            IsSuccess: true,
            Message: 'Issue Tracker Details has been updated successfully',
            data: post
        });
    });
});
router.post('/api/saveIssueTracking', function (req, res) {
    console.log(req.body)
    IssueTrackingMaster.create(req.body, function (err, post) {
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
        } else {
            res.json({
                IsSuccess: true,
                Message: 'Issue Tracker Details has been added successfully',
                data: post
            });
        }
    });
});
// create Build and send back all builds after creation
router.post('/api/addBuildDetails', function (req, res) {
    //console.log(req.body)
    BuildConfigurationsSchemaMaster.create(req.body, function (err, post) {
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
        } else {
            res.json({
                IsSuccess: true,
                Message: 'Build details has been added successfully',
                data: post
            });
        }
    });
});

// create a Build, information comes from AJAX request from Angular        
router.put('/api/updateBuildDetails/:_id', function (req, res) {
    BuildConfigurationsSchemaMaster.update({
        _id: req.params._id
    }, {
            IsActive: req.body.BuildDetails.IsActive,
            //  _projectId: req.body.BuildDetails._projectId,
            _repositoryId: req.body.BuildDetails._repositoryId,
            BuildRequisition: req.body.BuildDetails.BuildRequisition,
            BuildDefinition: req.body.BuildDetails.BuildDefinition,
            BuildConfiguration: req.body.BuildDetails.BuildConfiguration,
            BuildUser: req.body.BuildDetails.BuildUser,
            Password: req.body.BuildDetails.Password,
            BuildTool: req.body.BuildDetails.BuildTool,
            ModifiedBy: req.body.BuildDetails.ModifiedBy,
            ModifiedDate: req.body.BuildDetails.ModifiedDate

        }, req.body, function (err, numberAffected, rawResponse) {
            if (err) {
                res.json({
                    IsSuccess: false,
                    Message: err,
                    Data: err
                });
            } else {
                res.json({
                    IsSuccess: true,
                    Message: "Build Details has been updated successfully",
                    Data: err
                });
            }
        })
});


// delete a Build 
router.delete('/api/deleteBuildDetails/:_id', function (req, res) {
    console.log(req.params._id);
    BuildConfigurationsSchemaMaster.update({
        _id: req.params._id
    }, {
            $set: {
                IsActive: false
            }
        }, function (err, result) {
            if (err)
                res.send(err);
            else
                res.json({
                    IsSuccess: true,
                    Message: "Build details has been deleted successfully"
                });
        });
});

router.get('/api/getInstanceDetailsById/:_id', function (req, res) {
    InstanceTypeDetailMaster.find({
        IsActive: true,
        _id: req.params._id
    }, function (err, instances) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
            //res.send(err);.populate('_regionDetailId').populate('_cloudEnvironmentId')
        } else {
            res.json(instances);

        }

        // return all instances in JSON format
    });
});

router.get('/api/getRegionDetailsByRegionId/:_id', function (req, res) {
    OSDetailMaster.find({
        IsActive: true,
        _regionDetailId: req.params._id
    }).populate('_regionDetailId').exec(function (err, regions) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
            //res.send(err);.populate('_regionDetailId').populate('_cloudEnvironmentId')
        } else {
            res.json(regions);

        }

        // return all users in JSON format
    });
});

router.get('/api/getInstanceByEnvironmentId/:_id', function (req, res) {
    InstanceTypeDetailMaster.find({
        IsActive: true,
        _cloudEnvironmentId: req.params._id
    }).populate('_cloudEnvironmentId').exec(function (err, instances) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
            //res.send(err);.populate('_regionDetailId').populate('_cloudEnvironmentId')
        } else {
            res.json(instances);

        }

        // return all instances in JSON format
    });
});


router.get('/api/getRegionByEnvironmentId/:_id', function (req, res) {
    RegionDetailMaster.find({
        IsActive: true,
        _cloudEnvironmentId: req.params._id
    }).populate('_cloudEnvironmentId').exec(function (err, regions) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
            //res.send(err);.populate('_regionDetailId').populate('_cloudEnvironmentId')
        } else {
            res.json(regions);

        }

        // return all users in JSON format
    });
});


router.get('/api/getCloudEnvironments', function (req, res) {
    CloudEnvironmentMaster.find({
        IsActive: true
    }, function (err, environments) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
            //res.send(err);.populate('_regionDetailId').populate('_cloudEnvironmentId')
        } else {
            res.json(environments);

        }

        // return all users in JSON format
    });
});



router.post('/api/getInstanceReportDetails', function (req, res) {

    // Load the AWS SDK for Node.js
    var AWS = require('aws-sdk');    

    AWS.config.update({
        accessKeyId: configFile.accessKeyId,
        secretAccessKey: configFile.secretAccessKey,
        "region": req.body.RegionCode
    });

    var cloudwatch = new AWS.CloudWatch();

    var params = {
        EndTime: req.body.EndTime, //new Date(), // || 'Wed Dec 31 1969 16:00:00 GMT-0800 (PST)' || 123456789, /* required */
        MetricName: req.body.MatricName,
        /* required */
        Namespace: 'AWS/EC2',
        /* required */
        Period: req.body.Period,
        /* required */
        StartTime: req.body.StartTime, // new Date, // || 'Wed Dec 31 1969 16:00:00 GMT-0800 (PST)' || 123456789, /* required */
        Dimensions: [{
            Name: 'InstanceId',
            /* required */
            Value: req.body.InstanceId /* required */
        },
            /* more items */
        ],
        // ExtendedStatistics: [
        //   'STRING_VALUE',
        //   /* more items */
        // ],
        Statistics: [
            req.body.Statistic, // SampleCount | Average | Sum | Minimum | Maximum,
            /* more items */
        ],
        //Unit: "Seconds", //| Microseconds | Milliseconds | Bytes | Kilobytes | Megabytes | Gigabytes | Terabytes | Bits | Kilobits | Megabits | Gigabits | Terabits | Percent | Count | Bytes/Second | Kilobytes/Second | Megabytes/Second | Gigabytes/Second | Terabytes/Second | Bits/Second | Kilobits/Second | Megabits/Second | Gigabits/Second | Terabits/Second | Count/Second | None
    };
    cloudwatch.getMetricStatistics(params, function (err, data) {
        if (err) {
            res.json({
                IsSuccess: false,
                Message: (err),
                Data: data
            });
        } // an error occurred
        else {
            console.log("Success " + data); // successful response
            res.json({
                IsSuccess: true,
                Message: ("Instance created successsfully with Instance ID - "),
                Data: data
            });
        }
    });

});

// Code for getting AWS Instance Status
router.post('/api/getAWSInstanceDetails', function (req, res) {
    RegionDetailMaster.findOne({
        _id: req.body.RegionId,
        IsActive: true
    }, function (err, region) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
            //res.send(err);
        } else {

            var AWS = require('aws-sdk');            

            AWS.config.update({
                accessKeyId: configFile.accessKeyId,
                secretAccessKey: configFile.secretAccessKey,
                region: region.RegionCode
            });
            // AWS.config.loadFromPath('../Cloud Flex Development/config.json');
            var ec2 = new AWS.EC2({
                apiVersion: '2016-11-15'
            });
            // AWS.config.update({region: region.RegionCode}); 
            var params = {
                InstanceIds: [ /* required */
                    req.body.InstanceId,
                    /* more items */
                ],
                //AdditionalInfo: 'STRING_VALUE',
                //DryRun: true || false
            };
            ec2.describeInstances(params, function (err, awsInstancesDetails) {
                if (err) console.log(err, err.stack); // an error occurred
                else {
                    console.log(awsInstancesDetails);

                    res.json({
                        IsSuccess: true,
                        Data: awsInstancesDetails,
                        Message: ""
                    });

                } // successful response

            });

        }
    });

    // use mongoose to get all projects in the database
});

router.post('/api/refreshInstanceStatus', function (req, res) {
    RegionDetailMaster.findOne({
        _id: req.body.RegionId,
        IsActive: true
    }, function (err, region) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
            //res.send(err);
        } else {

            var AWS = require('aws-sdk');            

            AWS.config.update({
                accessKeyId: configFile.accessKeyId,
                secretAccessKey: configFile.secretAccessKey,
                region: region.RegionCode
            });
            // AWS.config.loadFromPath('../Cloud Flex Development/config.json');
            var ec2 = new AWS.EC2({
                apiVersion: '2016-11-15'
            });
            // AWS.config.update({region: region.RegionCode}); 
            var params = {
                InstanceIds: [ /* required */
                    req.body.InstanceId,
                    /* more items */
                ],
                //AdditionalInfo: 'STRING_VALUE',
                //DryRun: true || false
            };
            ec2.describeInstances(params, function (err, awsInstancesDetails) {
                if (err) console.log(err, err.stack); // an error occurred
                else {
                    console.log(awsInstancesDetails);


                                  if (awsInstancesDetails.Reservations.length > 0) {
                                    if (awsInstancesDetails.Reservations[0].Instances[0].State.Code == 48 || (awsInstancesDetails.Reservations[0].Instances[0].State.Name == "terminated" || awsInstancesDetails.Reservations[0].Instances[0].State.Name == "shutting-down")) {
                                        console.log(req.body.status);
                                        VMProjectEnvironmentConfigurationMaster.update({
                                            InstanceId: req.body.InstanceId
                                        }, {  $set:{
                                            InstanceStatus: req.body.status
                                        }
                                          
                                    
                                        },  function (err, numberAffected, rawResponse) {
                                            if (err) {
                                                return next(err);
                                            } else {
                                                res.json({
                                                    IsSuccess: true,
                                                        Message: "Instance status has been updated successfully"
                                                    });
                                            }
                                        })
                                    }
                                }
                                else {
                                    console.log(req.body.status);
                                    VMProjectEnvironmentConfigurationMaster.update({
                                        InstanceId: req.body.InstanceId
                                    }, {  $set:{
                                        InstanceStatus: req.body.status
                                    }
                                      
                                
                                    },  function (err, numberAffected, rawResponse) {
                                        if (err) {
                                            return next(err);
                                        } else {
                                            res.json({
                                                IsSuccess: true,
                                                    Message: "Instance status has been updated successfully"
                                                });
                                        }
                                    })
                                }
                         
                    // res.json({
                    //     IsSuccess: true,
                    //     Data: awsInstancesDetails,
                    //     Message: ""
                    // });
                    
                } // successful response

            });

        }
    });

    // use mongoose to get all projects in the database
});
//Notifications CRUD API's

router.post('/api/addNotificationDetails', function (req, res, next) {  

    NotificationSchemaMaster.create(req.body, function (err, post) {
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err
            });
        } else {
            res.json({
                IsSuccess: true,
                Message: "Notification saved successfully",
                Data: post
            });
        }
    });
});

router.put('/api/updateNotificationStatus/:_id', function (req, res) {
    console.log(req.params._id);
    NotificationSchemaMaster.update({
        _id: req.params._id
    }, {
        $set: {
            IsRead: true,
            IsDevRead: true
        }
    }, function (err, post) {
        if (err)
            res.send(err);
        else
        NotificationSchemaMaster.findOne({
            _id: req.params._id,
            IsActive: true       
        }).populate('_notifyTo').populate('_notifyFrom').populate('_projectId').populate('_buildId').exec(function (err, result) {
            if (err) {
                res.json({
                    IsSuccess: false,
                    Message: err,
                    Data: err
                });
            }
            else {
                res.json({
                    IsSuccess: true,
                    Message: "Success",
                    Data: result
                });
            }
        });
    });
});
router.put('/api/updateNotificationBuildStatus/:_id', function (req, res) {
    console.log(req.params._id);
    NotificationSchemaMaster.update({
        _id: req.params._id
    }, {
        $set: {
            Status: 'Completed'
        }
    }, function (err, post) {
        if (err)
            res.send(err);
        else
        NotificationSchemaMaster.findOne({
            _id: req.params._id,
            IsActive: true       
        }).populate('_notifyTo').populate('_notifyFrom').populate('_projectId').populate('_buildId').exec(function (err, result) {
            if (err) {
                res.json({
                    IsSuccess: false,
                    Message: err,
                    Data: err
                });
            }
            else {
                res.json({
                    IsSuccess: true,
                    Message: "Success",
                    Data: result
                });
            }
        });
    });
});
router.put('/api/updateNotificationDetails/:_id', function (req, res) {
    console.log(req.params._id);    
    NotificationSchemaMaster.update({
        _id: req.params._id
    }, {
        $set: {
            Status: req.body.Status,
            IsDevRead: false,
            Comment: req.body.Comment,
            ModifiedBy: req.body.ModifiedBy,
            ModifiedDate: new Date()            
        }
    }, function (err, post) {
        if (err)
            res.send(err);
        else        
        BuildMaster.update({
            _id: req.body._buildId._id
        }, {
            $set: {
                Status: req.body.Status,                
                ModifiedBy: req.body.ModifiedBy,
                ModifiedDate: new Date()            
            }
        }, function (err, result) {
    
            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err) {
                return next(err);
            }    
           else{
            res.json({
                IsSuccess: true,
                Message: "Notification updated successfully",
                Data: post
            });
           }
        });

        // var notifyDetails = {
        //     _notifyTo: req.body._notifyTo
        //     , _notifyFrom: req.body._notifyFrom,
        //     _projectId: req.body._projectId._id,
        //     _buildId: req.body._buildId._id,
        //     NotificationType: req.body.NotificationType,
        //     ErrorText: req.body.ErrorText,
        //     ErrorFilepath: req.body.ErrorFilepath,
        //     Description: req.body.Description,
        //     Comment: req.body.Comment,
        //     Status: req.body.Status,
        //     Priority: req.body.Priority,
        //     ReadOnTime: null,
        //     IsRead: false,
        //     IsActive: true,
        //     CreatedBy: req.body.CreatedBy,
        //     ModifiedBy: req.body.ModifiedBy,
        //     CreatedDate: new Date(),
        //     ModifiedDate: new Date()
        //   }
        // NotificationSchemaMaster.create(notifyDetails, function (err, result) {
        //     if (err) {
        //         res.json({
        //             IsSuccess: false,
        //             Message: err
        //         });
        //     } else {
        //         res.json({
        //             IsSuccess: true,
        //             Message: "Notification updated successfully",
        //             Data: result
        //         });
        //     }
        // });
    });
});

router.post('/api/getNotificationDetailsByUserId', function (req, res, next) {
    NotificationSchemaMaster.find({
        $or: [{
            _notifyTo: req.body._notifyTo
        },
        {
            _notifyFrom: req.body._notifyFrom
        }
    ],
    _projectId: req.body._projectId,
        IsActive: true       
    }).populate('_notifyTo').populate('_notifyFrom').populate('_projectId').populate('_buildId').exec(function (err, result) {
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
        }
        else {
            res.json({
                IsSuccess: true,
                Message: "Success",
                Data: result
            });
        }
    })
});


router.get('/api/getNotificationDetails', function (req, res) {
    NotificationSchemaMaster.find({
        IsActive: true
    }).populate('_notifyTo').populate('_notifyFrom').populate('_projectId').populate('_buildId').exec(function (err, result) {       
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
        }
        else {
            res.json({
                IsSuccess: true,
                Message: "Success",
                Data: result
            });
        }
    })
});



//Jira Integration API's
router.get('/api/createJiraProject', function (req, res) {

    var JiraApi = require('jira').JiraApi;

    var jira = new JiraApi('http', 'localhost', '8080', 'jeyakrishna21', 'jk@12345', 'latest');

    var linkData = {
        "assigneeType": "PROJECT_LEAD",
        "avatarId": null,
        "categoryId": null,
        "description": "test",
        "issueSecurityScheme": null,
        "key": "VAL",
        "lead": "jeyakrishna21",
        "name": "Valholla",
        "permissionScheme": null,
        "notificationScheme": null,
        "projectTypeKey": "business",
        "projectTemplateKey": "com.atlassian.jira-core-project-templates:jira-core-project-management",
        "url": "",

    };
    var options = {
        rejectUnauthorized: jira.strictSSL,
        uri: jira.makeUri('/project'),
        method: 'POST',
        json: true,
        body: linkData,

    };

    jira.doRequest(options, function (error, response) {
        console.log(response);
        res.json({
            IsSuccess: true,
            Message: "Success"
        });
    });


});

router.get('/api/createJiraIssues', function (req, res) {

    var JiraApi = require('jira').JiraApi;

    var jira = new JiraApi('http', 'localhost', '8080', 'jeyakrishna21', 'jk@12345', 'latest');
    //create bugs to projects

    var linkData = {
        "fields": {
            "project": {
                "key": "VAL"
            },
            "summary": "user name does not exists",
            "description": "",
            "issuetype": {
                "name": "Task"
            },
            "priority": {
                "name": "Medium"
            }
        }
    };


    var options = {
        rejectUnauthorized: jira.strictSSL,
        uri: jira.makeUri('/issue'),
        method: 'POST',
        json: true,
        body: linkData,

    };

    jira.doRequest(options, function (error, response) {
        console.log(response);
        res.json({
            IsSuccess: true,
            Message: "Success"
        });
    });

});

router.get('/api/findJiraIssues', function (req, res) {

    var JiraApi = require('jira').JiraApi;

    var jira = new JiraApi('http', 'localhost', '8080', 'jeyakrishna21', 'jk@12345', 'latest');
    //find bugs tracker

    var options = {
        rejectUnauthorized: jira.strictSSL,
        uri: jira.makeUri('/issue/' + 'VAL-1'),
        method: 'GET'
    };

    router.doRequest(options, function (error, response, body) {
        console.log(JSON.parse(body).fields.summary);
        res.json({
            IsSuccess: true,
            Message: "Success",
            Data: JSON.parse(body).fields.summary
        });
    });


});


router.put('/api/deleteJiraProject', function (req, res) {

    var JiraApi = require('jira').JiraApi;

    var jira = new JiraApi('http', 'localhost', '8080', 'jeyakrishna21', 'jk@12345', 'latest');
    //find bugs tracker

    var options = {
        rejectUnauthorized: jira.strictSSL,
        uri: jira.makeUri('/project/' + 'RMS'),
        method: 'PUT'
    };

    jira.doRequest(options, function (error, response, body) {
        res.json({
            IsSuccess: true,
            Message: "Success"
        });
    });

});


// var ActiveDirectory = require('activedirectory');
//     var config = { url: 'ldap://13.127.202.133', //ldap://GMI-DC.GlobalMantrai.com
//            baseDN: 'dc=valhollatesting,dc=com', //dc=GlobalMantrai,dc=com
//            //username: 'globalmantrai\\gjeyakrishnaraja',
//             //password: 'Pulsar220',                
//             }
//         var ad = new ActiveDirectory(config);

//         ad.authenticate('jk@valhollatesting.com', 'welcome@12', function(err, auth) {
//             console.log(err)
//             if (err) {
//               console.log('ERROR: '+JSON.stringify(err));
//               return;
//             }

//             if (auth) {
//               console.log('Authenticated!');
//               var opts = {
//                 bindDN: 'valhollatesting\\Administrator',
//                 bindCredentials: 'rnjpGll!2ih(5sK3KzhmV43trd$w?T!N'
//               };
//               ad.findUser(opts, 'jk', function(err, user) {
//                 if (err) {
//                   console.log('ERROR: ' +JSON.stringify(err));
//                   return;
//                 }

//                 if (! user) console.log('User: ' + 'globalmantrai\\smanoj' + ' not found.');
//                 else console.log(JSON.stringify(user));
//               });

//             }
//             else {
//               console.log('Authentication failed!');
//             }
//           });

module.exports = router;