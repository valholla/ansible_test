var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var path = require('path');
var http = require('http');

var RepositoryMaster = require('../models/repositoryschema.js');
var BuildConfigurationsSchemaMaster = require('../models/BuildConfigurationsSchema.js');


//Function related to Repository Module
function getRepositoryDetails(req, res, next) {
    RepositoryMaster.find({
        IsActive: true
    }).populate('_projectId').exec(function (err, repositories) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            return next(err);
        }

        res.json(repositories); // return all Repository in JSON format
    });
};


// get all Repositories
router.get('/api/getRepositoryDetails', function (req, res, next) {
    // use mongoose to get all repositories in the database

    getRepositoryDetails(req, res, next);
});
router.get('/api/getRepositoryDetailByProjectId/:_id', function (req, res, next) { 
    RepositoryMaster.find({
        IsActive: true,
        _projectId:req.params._id
    }).populate('_projectId').exec(function (err, repositories) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            return next(err);
        }

        res.json(repositories); // return all Repository in JSON format
    });
});
router.get('/api/getRepositoryDetailsById/:_id', function (req, res, next) {
    // use mongoose to get all repositories in the database
    console.log(req.params._id)
    RepositoryMaster.findOne({
        IsActive: true,
        _id:req.params._id
    }).populate('_projectId').exec(function (err, repositories) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            return next(err);
        }

        res.json(repositories); // return all Repository in JSON format
    });
    
});
// create repository and send back all users after creation
router.post('/api/addRepositoryDetails', function (req, res, next) {
    RepositoryMaster.create(req.body, function (err, post) {
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
        } else {
            res.json({
                IsSuccess: true,
                Message: 'New Repository has been added successfully',
                data: post
            });
        }
    });
    // return all Repository in JSON format

    });


// create a Repository, information comes from AJAX request from Angular        
router.put('/api/updateRepositoryDetails/:_id', function (req, res, next) {
    RepositoryMaster.update({
        _id: req.params._id
    }, {
        IsActive: req.body.RepositoryDetails.IsActive,
        Project: req.body.RepositoryDetails.Project,
        Repository: req.body.RepositoryDetails.Repository,
        Url: req.body.RepositoryDetails.Url,
        ModifiedBy: req.body.RepositoryDetails.ModifiedBy,
        ModifiedDate: req.body.RepositoryDetails.ModifiedDate

    }, req.body, function (err, numberAffected, rawResponse) {
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
        } else {
            res.json({
                IsSuccess: true,
                Message: "Repository Details has been updated successfully",
                Data: err
            });
        }
    })
});
    
 // delete a Repository Details
 router.delete('/api/deleteRepositoryById/:_id', function (req, res, next) {   
    RepositoryMaster.findOne({
        _id: req.params._id,
        IsActive: true
    }, function (err, post) {    
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {                
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });                
        } else{
            RepositoryMaster.update({
                _id: req.params._id
            }, {
                $set: {
                    IsActive: false
                }
            }, function (err, response) {
                if (err)
                    return next(err);
                else
                
                BuildConfigurationsSchemaMaster.updateMany({
                    _repositoryId: req.params._id,
                    _projectId:post._projectId
                }, {
                    $set: {
                        IsActive: false
                    }
                }, function (err, result) {
                    if (err)
                        return next(err);
                    else
                        res.json({
                            IsSuccess: true,
                            Message: "Repository has been deleted successfully"
                        });
                });
            });
        }
    });
});


module.exports = router;