var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var path = require('path');
var http = require('http');

var CloudServiceConfigureMaster = require('../models/CloudServiceConfigureSchema');


// create cloud configure and send back all users after creation
router.post('/api/saveCloudServiceConfigurationDetails', function (req, res, next) {
    CloudServiceConfigureMaster.create(req.body, function (err, post) {
            if (err) {
                res.json({
                    IsSuccess: false,
                    Message: err,
                    Data: err
});
            } else {
                res.json({
                    IsSuccess: true,
                    Message: 'Cloud Service Configuration has been added successfully',
                    Data: post
                });
            }
        });
    // return all cloud configure in JSON format

});

router.post('/api/updateCloudServiceConfigurationDetails/:_id', function (req, res) {    
    CloudServiceConfigureMaster.update({
        _id: req.params._id
    }, {
        $set: req.body
    }, function (err, post) {
        if (err)
            res.send(err);
        else
        res.json({
            IsSuccess: true,
            Message: "Cloud Service Configuration has been updated successfully",
            Data: post
        });
    });
});

router.get('/api/getCloudServiceConfigurationDetails', function (req, res) {
    CloudServiceConfigureMaster.findOne({
        IsActive: true
    }, function (err, result) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
            
        } else {
            res.json({
                IsSuccess: true,
                Message: 'Success',
                Data: result
            });
        }

        // return all cloud service details in JSON format
    });
});


module.exports = router;