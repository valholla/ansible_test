var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var path = require('path');
var http = require('http');

var EnvironmentMaster = require('../models/EnvironmentConfigureSchema.js');
var VMProjectEnvironmentConfigurationMaster = require('../models/VMProjectEnvironmentConfigurationSchema.js');
var deleteProject = require("./InstanceCRUD");
var manager = new deleteProject();


//Function related to Environment Module
function getEnvironmentDetailById(req, res, next) {
    EnvironmentMaster.find({
        IsActive: true,
        _id:req.params.environmeId
    }).exec(function (err, result) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            return next(err);
        }
        res.json(result); // return all Environment in JSON format
    });
};

//Function related to Environment Module
function getEnvironmentDetails(req, res, next) {
    EnvironmentMaster.find({
        IsActive: true
    }).exec(function (err, result) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            return next(err);
        }
        res.json(result); // return all Environment in JSON format
    });
};
router.get('/api/getEnvironmentDetailById/:environmeId',function (req, res, next) {
    // use mongoose to get all projects in the database

    getEnvironmentDetailById(req, res, next);
});
 // get all enviorments
 router.get('/api/getEnvironmentDetails', function (req, res, next) {
    // use mongoose to get all projects in the database

    getEnvironmentDetails(req, res, next);
});

// create environment and send back all users after creation
router.post('/api/saveEnvironmentDetails', function (req, res, next) {
    EnvironmentMaster.create(req.body, function (err, post) {
            if (err) {
                res.json({
                    IsSuccess: false,
                    Message: err,
                    Data: err
});
            } else {
                res.json({
                    IsSuccess: true,
                    Message: 'New Environment has been added successfully',
                    data: post
                });
            }
        });
    // return all environments in JSON format

});


// create a Environment, information comes from AJAX request from Angular        
router.put('/api/updateEnvironmentDetails/:_id', function (req, res, next) {
    EnvironmentMaster.update({
        _id: req.params._id
    }, {
            IsActive: req.body.EnvironmentDetails.IsActive,
        EnvironmentName: req.body.EnvironmentDetails.environmentName,
        EnvironmentDescription: req.body.EnvironmentDetails.environmentDescription,
            ModifiedBy: req.body.EnvironmentDetails.ModifiedBy,
            ModifiedDate: req.body.EnvironmentDetails.ModifiedDate            
            
        }, req.body, function (err, numberAffected, rawResponse) {
            if (err) {
                res.json({
                    IsSuccess: false,
                    Message: err,
                    Data: err
                });
        } else {
                res.json({
                    IsSuccess: true,
                    Message: "Environment has been updated successfully",
                    Data: err
                });
            }
        })
        });


        // delete a Environment Details
    router.delete('/api/deleteEnvironmentById/:_id', function (req, res, next) {        
        EnvironmentMaster.update({
            _id: req.params._id
        }, {
            $set: {
                IsActive: false
            }
        }, function (err, result) {
            if (err)
                return next(err);
            else
            
            VMProjectEnvironmentConfigurationMaster.find({
                IsActive: true,
                _environmentConfigureId: req.params._id
            }).populate('_projectId').populate('_cloudEnvironmentId').populate('_VMTemplateDetailId').populate('_environmentConfigureId').exec(function (err, projectVM) {
                
                // if there is an error retrieving, send the error. nothing after res.send(err) will execute
                if (err) {
                    res.json({
                        IsSuccess: false,
                        Message: err,
                        Data: err
                    });
                }
                else {
                    // res.json({
                    //     IsSuccess: true,
                    //     Message: "Environment has been deleted successfully"
                    // });
                    
                    manager.getRegionDetails(projectVM,req,res);
                    // res.json(projectVM); // return all projectVM in JSON format

                }

            });
              
        });
    }); 
    


module.exports = router;