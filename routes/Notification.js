﻿var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var path = require('path');
var http = require('http');
var configFile = require('../config.json');
var UserMaster = require('../models/userschema.js');

router.get('/api/sendsmsnotification/:phonenumber',function (req, res, next) {    
    sendSMSNotification(req, res, next);
});

//Send SMS Notification
function sendSMSNotification(req, res, next) {
    UserMaster.findOne({
        $or: [{
            UserName: {
                $regex: new RegExp("^" + "bruce", "i")
            }
        },
        {
            Email: {
                $regex: new RegExp("^" + "bruce", "i")
            }
        }
        ],
        IsActive: true
    }, function (err, user) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
            //res.send(err);
        }
        if (!user) {
            res.json({
                IsSuccess: false,
                Message: 'Username or Email Id does not exists',
                Data: null
            });
        } else {

            console.log(user)
            var configFile = require('../config.json');

            // Load the AWS SDK for Node.js
            var AWS = require('aws-sdk');
            AWS.config.update({
                accessKeyId: configFile.accessKeyId, //"AKIAITAAUVFATOV3ICIA"
                secretAccessKey: configFile.secretAccessKey, //"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"
                "region": configFile.region // "ap-southeast-1"  
            });

            var approveLink = "https://www.valholla.com/notification/792/approve/74lO0pIo87EhT";
            var rejectLink = "https://www.valholla.com/notification/792/reject/74lO0pIo87EhT";

            let params = {
                Message: "Valholla - Hi "+user.FirstName+" "+user.LastName+". There is an new release for HRET. Kindly have a look on Valholla App for more details and Approve by clicking - " + approveLink + " or Reject by clicking - " + rejectLink, /* required */
                PhoneNumber: "+1"+ user.Mobile, // "+919750622146",
            };

            // Create promise and SNS service object
            var publishTextPromise = new AWS.SNS({ apiVersion: '2010-03-31' }).publish(params).promise();

            // Handle promise's fulfilled/rejected states
            publishTextPromise.then(
                function (data) {
                    console.log("MessageID is " + data.MessageId);
                    msgId = data.MessageId;
                    res.json("SMS Sent with msg id -" + data.MessageId);
                }).catch(
                function (err) {
                    console.error(err, err.stack);
                    res.json("SMS Not Sent -" + err);
                });
        }

        // return all users in JSON format
    });
    // var configFile = require('../config.json');

    // // Load the AWS SDK for Node.js
    // var AWS = require('aws-sdk');
    // AWS.config.update({
    //     accessKeyId: configFile.accessKeyId, //"AKIAITAAUVFATOV3ICIA"
    //     secretAccessKey: configFile.secretAccessKey, //"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"
    //     "region": configFile.region // "ap-southeast-1"  
    // });

    // var approveLink = "https://www.valholla.com/notification/792/approve/74lO0pIo87EhT";
    // var rejectLink  = "https://www.valholla.com/notification/792/reject/74lO0pIo87EhT";

    // let params = {
    //     Message: "Valholla - Hi Manoj. There is an new release for HRET. Kindly have a look on Valholla App for more details and Approve by clicking - "+ approveLink +" or Reject by clicking - " + rejectLink , /* required */
    //     PhoneNumber: configFile.NotifyToPhoneNumber, // "+919750622146",
    // };

    // // Create promise and SNS service object
    // var publishTextPromise = new AWS.SNS({apiVersion: '2010-03-31'}).publish(params).promise();

    // // Handle promise's fulfilled/rejected states
    // publishTextPromise.then(
    // function(data) {
    //     console.log("MessageID is " + data.MessageId);
    //     msgId = data.MessageId;
    //     res.json("SMS Sent with msg id -" + data.MessageId);
    // }).catch(
    //     function(err) {
    //     console.error(err, err.stack);
    //     res.json("SMS Not Sent -" + err);
    // });
};
module.exports = router;
