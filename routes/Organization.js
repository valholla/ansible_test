var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var path = require('path');
var http = require('http');

var OrganizationMaster = require('../models/OrganizationSchema.js');



router.post('/api/saveOrganizationDetails', function (req, res, next) {
    OrganizationMaster.create(req.body, function (err, post) {
        if (err)
            res.send(err);
        if (err) {
        
             res.json({
                 IsSuccess: false,
                 Message: err
             });
        } else {
             res.json({
                 IsSuccess: true,
                 Message: "Organization has been added successfully"
             });
         }
    });
});

router.put('/api/updateOrganizationDetails/:_id', function (req, res, next) {    

    OrganizationMaster.findOne({
        _id: req.params._id,
        IsActive: true
    }, function (err, result) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
            
        } else {
            OrganizationMaster.update({
                _id: req.params._id
           }, 
           {
               $set: req.body
            }, function (err, post) {
               if (!result) {
                   res.json({
                       IsSuccess: false,
                       Message: err
                   });
               } else {
                   res.json({
                       IsSuccess: true,
                       Message: "Organization has been updated successfully",
                       Data: post
                   });
               }
           });
        }
    });
    
});

router.get('/api/getOrganizationDetails', function (req, res, next) {
    // use mongoose to get all organization from the database
    OrganizationMaster.find({
        IsActive: true       
    }, function (err, result) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
            
        } else {
            res.json({
                IsSuccess: true,
                Message: "Success",
                Data: result
            });
        }

        // return all organization in JSON format
    });
  
});

router.get('/api/getOrganizationDetailById/:_id', function (req, res, next) {
    // use mongoose to get all organization from the database
    OrganizationMaster.findOne({
        IsActive: true,
        _id: req.params._id
    }, function (err, result) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
            
        } else {
            res.json({
                IsSuccess: true,
                Message: "Success",
                Data: result
            });
        }

        // return all organization in JSON format
    });
  
});



module.exports = router;