var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var path = require('path');
var http = require('http');

var UserProjectRoleMaster = require('../models/userprojectroleschema.js');



router.post('/api/saveResourceDetails', function (req, res, next) {
    UserProjectRoleMaster.create(req.body, function (err, post) {
        if (err)
            res.send(err);
        if (err) {
        
             res.json({
                 IsSuccess: false,
                 Message: err
             });
        } else {
             res.json({
                 IsSuccess: true,
                 Message: "New Resource has been added successfully"
             });
         }
    });
});

router.put('/api/updateResourceDetails', function (req, res, next) {    
    UserProjectRoleMaster.update({
        _id: req.body._id
    }, {
        IsActive: req.body.IsActive,
        _userId: req.body._userId,
        _roleId: req.body._roleId,
        _projectId: req.body._projectId,
        IsPrimaryManager: false,
        // CreatedBy: $scope.localUserDetails._id,
        // CreatedDate: Date.now(),
        ModifiedBy: req.body.ModifiedBy,
        ModifiedDate: req.body.ModifiedDate,

    }, req.body, function (err, numberAffected, rawResponse) {
        if (err) {
            res.json({
                IsSuccess: false,
                Message: err,
                Data: err
            });
        } else {
            res.json({
                IsSuccess: true,
                Message: "Resource has been updated successfully",
                Data: err
            });
        }
    })

});


router.delete('/api/deleteResourceById/:_id', function (req, res, next) {        
    var obj = {
        IsActive: false
    };
    UserProjectRoleMaster.update({
        _id: req.params._id           
    }, {
        $set: obj
    }).exec().then(function (result) {
        if (!result) {
            res.json({
                IsSuccess: false,
                Message: result,
                Data: result
            });
        } else {
            res.json({
                IsSuccess: true,
                Message: "Resource has been deleted successfully",
                Data: result
            });
        }
    })

});


module.exports = router;