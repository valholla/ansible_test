var mongoose = require("mongoose");
var autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);
var Schema = mongoose.Schema;

var RepositorySchema = new Schema({
  _projectId: { type: Number, default: 0, ref: 'Projects' }
, Repository: { type: String}  
, Url: { type: String} 
, UserName: { type: String,  trim: true }
, Password: { type: String,  trim: true }
, CreatedBy: { type: String }
, ModifiedBy: { type: String }
, IsActive: { type: Boolean, default: true }
, CreatedDate: { type: Date, default: Date.now }
, ModifiedDate: { type: Date, default: null }
});

module.exports = mongoose.model('Repository', RepositorySchema);

RepositorySchema.plugin(autoIncrement.plugin, {
    model: 'Repository',
    field: '_id',
    startAt: 1,
    incrementBy: 1
});