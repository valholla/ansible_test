var mongoose = require("mongoose");
var autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);
var Schema = mongoose.Schema;

var RolesSchema = new Schema({
	 RoleName: { type: String, unique: true, required: true, trim: true }
    , RoleDescription: { type: String, required: true, trim: true } 
    ,IsRoleStatus: { type: Boolean, default: true }
    ,IsActive: { type: Boolean, default: true }
    , CreatedBy: { type: String }
    , ModifiedBy: { type: String }
    , CreatedDate: { type: Date, default: Date.now }
    , ModifiedDate: { type: Date, default: Date.now }
  
  });
  

module.exports = mongoose.model('Roles', RolesSchema);

RolesSchema.plugin(autoIncrement.plugin, {
    model: 'Roles',
    field: '_id',
    startAt: 1,
    incrementBy: 1
});