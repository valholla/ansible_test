var mongoose = require("mongoose");
var autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);
var Schema = mongoose.Schema;

var UserRoleSchema = new Schema({
    _userId: { type: Number, default: 0, ref: 'Users' }
    ,_roleId: { type: Number, default: 0, ref: 'Roles' }    
    ,_projectId: { type: Number, default: 0, ref: 'Projects' }     
, IsActive: { type: Boolean, default: true }
, IsPrimaryManager: { type: Boolean, default: true }
, CreatedBy: { type: String }
, ModifiedBy: { type: String }
, CreatedDate: { type: Date, default: Date.now }
, ModifiedDate: { type: Date, default: null }
});

module.exports = mongoose.model('UserProjectRoles', UserRoleSchema);

UserRoleSchema.plugin(autoIncrement.plugin, {
    model: 'UserProjectRoles',
    field: '_id',
    startAt: 1,
    incrementBy: 1
});