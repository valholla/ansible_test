var mongoose = require("mongoose");
var autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);
var Schema = mongoose.Schema;

var CloudServiceConfigureSchema = new Schema({
    AccessKeyId: { type: String, required: true, trim: true }
    ,SecretAccessKey: { type: String, required: true, trim: true }
    ,Region: { type: String, required: true, trim: true }
    ,KeyName: { type: String, required: true, trim: true }
    , IsActive: { type: Boolean, default: true }   
, CreatedBy: { type: String }
, ModifiedBy: { type: String }
, CreatedDate: { type: Date, default: Date.now }
, ModifiedDate: { type: Date, default: null }
});

module.exports = mongoose.model('CloudServiceConfigure', CloudServiceConfigureSchema);

CloudServiceConfigureSchema.plugin(autoIncrement.plugin, {
    model: 'CloudServiceConfigure',
    field: '_id',
    startAt: 1,
    incrementBy: 1
});