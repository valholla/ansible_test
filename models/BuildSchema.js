var mongoose = require("mongoose");
var autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);
var Schema = mongoose.Schema;

var BuildSchema = new Schema({
    _projectId: {
        type: Number,
        default: 0,
        ref: 'Projects'
    },
    BuildVersion: {
        type: String,
        trim: true
    },
    BuildVersionType: {
        type: String,
        trim: true
    },
    _projectRepositoryId: {
        type: Number,
        trim: true,
        ref: 'Repository'
    },
    _buildConfigurationId: {
        type: Number,
        trim: true,
        ref: 'BuildConfigurations'
    },
    _environmentConfigureId: { 
        type: Number, 
        ref: 'EnvironmentConfigures' 
    },
    InstanceId: {
        type: String,
        trim: true
    },
    Status: { 
        type: String, 
        trim: true 
    },
    IsActive: {
        type: Boolean,
        default: true
    },
    CreatedBy: {
        type: String
    },
    ModifiedBy: {
        type: String
    },
    CreatedDate: {
        type: Date,
        default: Date.now
    },
    ModifiedDate: {
        type: Date,
        default: null
    },
    DeploymentUrl: {
        type: String,
        default: null
    },
    BuildRequisition: {
        type: String,
        default: null
    }
});

module.exports = mongoose.model('Builds', BuildSchema);

BuildSchema.plugin(autoIncrement.plugin, {
    model: 'Builds',
    field: '_id',
    startAt: 1,
    incrementBy: 1
});