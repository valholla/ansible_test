var mongoose = require("mongoose");
var autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);
var Schema = mongoose.Schema;

var EnvironmentConfigureSchema = new Schema({
    EnvironmentName: { type: String, required: true, trim: true }
    ,EnvironmentDescription: { type: String, required: true, trim: true }
    , IsActive: { type: Boolean, default: true }   
, CreatedBy: { type: String }
, ModifiedBy: { type: String }
, CreatedDate: { type: Date, default: Date.now }
, ModifiedDate: { type: Date, default: null }
});

module.exports = mongoose.model('EnvironmentConfigures', EnvironmentConfigureSchema);

EnvironmentConfigureSchema.plugin(autoIncrement.plugin, {
    model: 'EnvironmentConfigures',
    field: '_id',
    startAt: 1,
    incrementBy: 1
});