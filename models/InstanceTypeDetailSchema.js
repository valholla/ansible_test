var mongoose = require("mongoose");
var autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);
var Schema = mongoose.Schema;

var InstanceTypeDetailSchema = new Schema({  
    _cloudEnvironmentId: { type: Number, ref: 'CloudEnvironments' }
    ,_regionDetailId: { type: Number, ref: 'RegionDetails' }
    ,InstanceTypeName: { type: String, required: true, trim: true }
    ,InstanceDescription: { type: String, required: true, trim: true }
    ,APIName: { type: String, required: true, trim: true }
    ,Memory: { type: String, required: true, trim: true }
    ,VCPU: { type: String, required: true, trim: true }
    ,InstanceStorage: { type: String, required: true, trim: true }
    ,NetworkPerformance: { type: String, required: true, trim: true }
    ,ClockSpeed: { type: String, trim: true }
    , IsActive: { type: Boolean, default: true }   
, CreatedBy: { type: String }
, ModifiedBy: { type: String }
, CreatedDate: { type: Date, default: Date.now }
, ModifiedDate: { type: Date, default: null }
});

module.exports = mongoose.model('InstanceTypeDetails', InstanceTypeDetailSchema);

InstanceTypeDetailSchema.plugin(autoIncrement.plugin, {
    model: 'InstanceTypeDetails',
    field: '_id',
    startAt: 1,
    incrementBy: 1
});