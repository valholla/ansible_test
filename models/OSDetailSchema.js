var mongoose = require("mongoose");
var autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);
var Schema = mongoose.Schema;

var OSDetailSchema = new Schema({  
    _regionDetailId: { type: Number, ref: 'RegionDetails' }
    ,OSType: { type: String, required: true, trim: true }
    ,OSImageId: { type: String, required: true, trim: true }
    , IsActive: { type: Boolean, default: true }   
, CreatedBy: { type: String }
, ModifiedBy: { type: String }
, CreatedDate: { type: Date, default: Date.now }
, ModifiedDate: { type: Date, default: null }
});

module.exports = mongoose.model('OSDetails', OSDetailSchema);

OSDetailSchema.plugin(autoIncrement.plugin, {
    model: 'OSDetails',
    field: '_id',
    startAt: 1,
    incrementBy: 1
});