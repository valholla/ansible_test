var mongoose = require("mongoose");
var autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);
var Schema = mongoose.Schema;

var ProjectSchema = new Schema({
    _userId: { type: Number, ref: 'Users' }
    ,ProjectName: { type: String, required: true, trim: true }
    ,ProjectDescription: { type: String, required: true, trim: true }
    , StartDate: { type: Date, default: Date.now }
    , EndDate: { type: Date, default: null }
, IsActive: { type: Boolean, default: true }
, CreatedBy: { type: String }
, ModifiedBy: { type: String }
, CreatedDate: { type: Date, default: Date.now }
, ModifiedDate: { type: Date, default: null }
});

module.exports = mongoose.model('Projects', ProjectSchema);

ProjectSchema.plugin(autoIncrement.plugin, {
    model: 'Projects',
    field: '_id',
    startAt: 1,
    incrementBy: 1
});