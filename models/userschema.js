var mongoose = require("mongoose");
var autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    UserName: { type: String,  required: true, trim: true }
, Email: { type: String, required: true, trim: true }
, Password: { type: String, required: true, trim: true }
, FirstName: { type: String, required: true, trim: true }
, LastName: { type: String, required: true, trim: true }
, Title: { type: String, trim: true }
, Mobile: { type: String, trim: true }

, userimageName: { type: String }
, userimageBytes: { type: String }
, userimageDirPath: { type: String }
, imagetype: { type: String }
, userOriginalImgName:{type: String}
, IsActive: { type: Boolean, default: true }
, IsEmailActivated: { type: Boolean, default: false }
, IsRequestedforResetPwd: { type: Boolean, default: false }
, IsRequestedforResetPwdTime: { type: Date, default: null }
, IsSuperAdmin: { type: Boolean, default: false }
, IsPrimaryManger: { type: Boolean, default: false }
, _roleId: { type: Number, default: 0, ref: 'Roles' }
, LastLoginTime: { type: Date, default: null }
, CreatedBy: { type: String }
, ModifiedBy: { type: String }
, CreatedDate: { type: Date, default: Date.now }
, ModifiedDate: { type: Date, default: null }
});

module.exports = mongoose.model('Users', UserSchema);

UserSchema.plugin(autoIncrement.plugin, {
    model: 'Users',
    field: '_id',
    startAt: 1,
    incrementBy: 1
});