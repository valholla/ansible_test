var mongoose = require("mongoose");
var autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);
var Schema = mongoose.Schema;

var BuildPhaseSchema = new Schema({
    _buildId: { type: Number, default: 0, ref: 'Builds' }       
    ,PhaseName: { type: String, required: true, trim: true } 
    ,PhaseDescription: { type: String, required: true, trim: true } 
    ,Progress: { type: String, trim: true } 
    ,PhaseStartTime: { type: Date, default: Date.now }
    ,PhaseEndTime: { type: Date, default: Date.now }
    ,PhaseLog: { type: String, trim: true } 
    ,Status: { type: String, trim: true } 
    ,StatusInfo: { type: String, trim: true } 
, IsActive: { type: Boolean, default: true }
, CreatedBy: { type: String }
, ModifiedBy: { type: String }
, CreatedDate: { type: Date, default: Date.now }
, ModifiedDate: { type: Date, default: null }
});

module.exports = mongoose.model('BuildPhases', BuildPhaseSchema);

BuildPhaseSchema.plugin(autoIncrement.plugin, {
    model: 'BuildPhases',
    field: '_id',
    startAt: 1,
    incrementBy: 1
});