var mongoose = require("mongoose");
var autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);
var Schema = mongoose.Schema;

var VMTemplateEnvironmentAssignSchema = new Schema({   
    _projectId: { type: Number, default: 0, ref: 'Projects' }
    ,_cloudEnvironmentId: { type: Number, ref: 'CloudEnvironments' }
    ,_VMTemplateDetailId: { type: Number, ref: 'VMTemplateDetails' }
    ,_environmentConfigureId: { type: Number, ref: 'EnvironmentConfigures' }
    , IsActive: { type: Boolean, default: true }
, CreatedBy: { type: String }
, ModifiedBy: { type: String }
, CreatedDate: { type: Date, default: Date.now }
, ModifiedDate: { type: Date, default: null }
});

module.exports = mongoose.model('VMTemplateEnvironmentAssigns', VMTemplateEnvironmentAssignSchema);

VMTemplateEnvironmentAssignSchema.plugin(autoIncrement.plugin, {
    model: 'VMTemplateEnvironmentAssigns',
    field: '_id',
    startAt: 1,
    incrementBy: 1
});