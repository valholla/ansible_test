var mongoose = require("mongoose");
var autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);
var Schema = mongoose.Schema;

var NotificationSchema = new Schema({     
    _notifyTo: { type: Number, default: 0, ref: 'Users' }   
    ,_notifyFrom: { type: Number, default: 0, ref: 'Users' }   
    , _projectId: { type: Number, default: 0, ref: 'Projects' }
    , _buildId: { type: Number, default: 0, ref: 'Builds' }
    ,NotificationType: { type: String, trim: true }  
    ,ErrorText: { type: String, trim: true }
    ,ErrorFilepath: { type: String, trim: true }
    ,Description: { type: String, trim: true }
    ,Comment: { type: String, trim: true }
    ,Status: { type: String, trim: true }
    ,Priority: { type: String, trim: true }
    ,ReadOnTime: { type: Date, default: Date.now }
    ,IsRead: { type: Boolean, default: true }
    ,IsDevRead: { type: Boolean, default: true }
, IsActive: { type: Boolean, default: true }
, CreatedBy: { type: String }
, ModifiedBy: { type: String }
, CreatedDate: { type: Date, default: Date.now }
, ModifiedDate: { type: Date, default: null }
});

module.exports = mongoose.model('Notification', NotificationSchema);

NotificationSchema.plugin(autoIncrement.plugin, {
    model: 'Notification',
    field: '_id',
    startAt: 1,
    incrementBy: 1
});