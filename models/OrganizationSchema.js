var mongoose = require("mongoose");
var autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);
var Schema = mongoose.Schema;

var OrganizationSchema = new Schema({
    OrganizationName: { type: String, required: true, trim: true } 
    ,OrganizationDescription: { type: String, trim: true } 
    ,PointOfContact: { type: String, trim: true } 
    ,Email: { type: String, trim: true }
    ,Phone: { type: String, trim: true }
    ,ConfigurationDate: { type: Date, default: Date.now }
    ,StartDate: { type: Date, default: Date.now }
    ,EndDate: { type: Date, default: Date.now }
, IsActive: { type: Boolean, default: true }
, CreatedBy: { type: String }
, ModifiedBy: { type: String }
, CreatedDate: { type: Date, default: Date.now }
, ModifiedDate: { type: Date, default: null }
});

module.exports = mongoose.model('Organization', OrganizationSchema);

OrganizationSchema.plugin(autoIncrement.plugin, {
    model: 'Organization',
    field: '_id',
    startAt: 1,
    incrementBy: 1
});