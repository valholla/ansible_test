var mongoose = require("mongoose");
var autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);
var Schema = mongoose.Schema;

var IssueTrackingSchema = new Schema({
    Url: { type: String, required: true, trim: true } 
    ,UrlPort: { type: String, trim: true } 
    ,UserName: { type: String, trim: true } 
    ,Password: { type: String, trim: true }
   
, IsActive: { type: Boolean, default: true }
, CreatedBy: { type: String,default: null }
, ModifiedBy: { type: String,default: null }
, CreatedDate: { type: Date, default: Date.now }
, ModifiedDate: { type: Date, default: null }
});

module.exports = mongoose.model('IssueTracking', IssueTrackingSchema);

IssueTrackingSchema.plugin(autoIncrement.plugin, {
    model: 'IssueTracking',
    field: '_id',
    startAt: 1,
    incrementBy: 1
});