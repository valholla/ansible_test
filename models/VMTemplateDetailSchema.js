var mongoose = require("mongoose");
var autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);
var Schema = mongoose.Schema;

var VMTemplateDetailSchema = new Schema({   
    TemplateName: { type: String, required: true, trim: true }
    ,TemplateDescription: { type: String, required: true, trim: true }     
    ,_cloudEnvironmentId: { type: Number, ref: 'CloudEnvironments' }
    ,_instanceTypeId: { type: Number, ref: 'InstanceTypeDetails' }
    ,_regionDetailId: { type: Number, ref: 'RegionDetails' }
   
    ,VMStatus: { type: String }
    ,_OSId: { type: Number, ref: 'OSDetails' }
    , IsActive: { type: Boolean, default: true }
    , IsSavedTemplate: { type: Boolean, default: true }
    , IsDirectSavedTemplate: { type: Boolean, default: true }
, CreatedBy: { type: String }
, ModifiedBy: { type: String }
, CreatedDate: { type: Date, default: Date.now }
, ModifiedDate: { type: Date, default: null }
});

module.exports = mongoose.model('VMTemplateDetails', VMTemplateDetailSchema);

VMTemplateDetailSchema.plugin(autoIncrement.plugin, {
    model: 'VMTemplateDetails',
    field: '_id',
    startAt: 1,
    incrementBy: 1
});