var mongoose = require("mongoose");
var autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);
var Schema = mongoose.Schema;

var RegionDetailSchema = new Schema({  
    RegionName: { type: String, required: true, trim: true }
    ,RegionDescription: { type: String, required: true, trim: true }
    ,RegionCode: { type: String, required: true, trim: true }
    ,_cloudEnvironmentId: { type: Number, ref: 'CloudEnvironments' }    
    , IsActive: { type: Boolean, default: true }   
, CreatedBy: { type: String }
, ModifiedBy: { type: String }
, CreatedDate: { type: Date, default: Date.now }
, ModifiedDate: { type: Date, default: null }
});

module.exports = mongoose.model('RegionDetails', RegionDetailSchema);

RegionDetailSchema.plugin(autoIncrement.plugin, {
    model: 'RegionDetails',
    field: '_id',
    startAt: 1,
    incrementBy: 1
});