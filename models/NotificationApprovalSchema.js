var mongoose = require("mongoose");
var autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);
var Schema = mongoose.Schema;

var NotificationApprovalSchema = new Schema({
    From: { type: String, required: true, trim: true } 
    ,To: { type: String, trim: true } 
    ,ServerName: { type: String, trim: true } 
    ,UserId: { type: String, trim: true } 
    ,Password: { type: String, trim: true }
   
, IsActive: { type: Boolean, default: true }
, CreatedBy: { type: String,default: null }
, ModifiedBy: { type: String,default: null }
, CreatedDate: { type: Date, default: Date.now }
, ModifiedDate: { type: Date, default: null }
});

module.exports = mongoose.model('NotificationApproval', NotificationApprovalSchema);

NotificationApprovalSchema.plugin(autoIncrement.plugin, {
    model: 'NotificationApproval',
    field: '_id',
    startAt: 1,
    incrementBy: 1
});