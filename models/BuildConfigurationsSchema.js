var mongoose = require("mongoose");
var autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);
var Schema = mongoose.Schema;

var BuildConfigurationsSchema = new Schema({
    _projectId: {
        type: Number,
        default: 0,
        ref: 'Projects'
    },
    _repositoryId: {
        type: Number,
        default: 0,
        ref: 'Repository'
    },
   
    BuildDefinition:
    {
        type: String,
        default: null
    },

    BuildConfiguration:
    {
        type: String,
        default: null
    },
    BuildUser:
    {
        type: String,
        default: null
    },
    Password:
    {
        type: String,
        default: null
    },
    BuildTool:
    {
        type: String,
        default: null
    },
    
    IsActive: {
        type: Boolean,
        default: true
    },
    CreatedBy: {
        type: String
    },
    ModifiedBy: {
        type: String
    },
    CreatedDate: {
        type: Date,
        default: Date.now
    },
    ModifiedDate: {
        type: Date,
        default: null
    },
    
   
});

module.exports = mongoose.model('BuildConfigurations', BuildConfigurationsSchema);

BuildConfigurationsSchema.plugin(autoIncrement.plugin, {
    model: 'BuildConfigurations',
    field: '_id',
    startAt: 1,
    incrementBy: 1
});