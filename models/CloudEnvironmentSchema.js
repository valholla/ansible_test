var mongoose = require("mongoose");
var autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);
var Schema = mongoose.Schema;

var CloudEnvironmentSchema = new Schema({
    CloudEnvironmentTypeName: { type: String, required: true, trim: true }
    ,CloudEnvironmentDescription: { type: String, required: true, trim: true }
    , IsActive: { type: Boolean, default: true }   
, CreatedBy: { type: String }
, ModifiedBy: { type: String }
, CreatedDate: { type: Date, default: Date.now }
, ModifiedDate: { type: Date, default: null }
});

module.exports = mongoose.model('CloudEnvironments', CloudEnvironmentSchema);

CloudEnvironmentSchema.plugin(autoIncrement.plugin, {
    model: 'CloudEnvironments',
    field: '_id',
    startAt: 1,
    incrementBy: 1
});