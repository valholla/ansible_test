const {
    spawn
} = require('child_process');

var buildmanager = {
    startBuild(buildid, userId, environmentId, isHavetoDeploy) {
        var child_process = spawn('node', ["buildengine/runbuild.js", buildid, userId, environmentId, isHavetoDeploy], {
            stdio: 'inherit'
        });
        console.log("Build Process started:" + child_process.pid)

        child_process.on('close', (code) => {
            console.log(`child process for build ${buildid} exited with code ${code}`);
        });

    }
};

module.exports = buildmanager;
