var mongoose = require('mongoose');

var BuildPhaseSchema = require('../models/BuildPhaseSchema');
var VMProjectEnvironmentConfigurationSchema = require('../models/VMProjectEnvironmentConfigurationSchema');
var VMTemplateDetailSchema = require('../models/VMTemplateDetailSchema');
var RegionDetailSchema = require('../models/RegionDetailSchema');
var EnvironmentConfigureSchema = require('../models/EnvironmentConfigureSchema');

var BuildSchema = require('../models/BuildSchema');
var ProjectSchema = require('../models/projectschema');
var mongoDB = 'mongodb://admin:valholla123@13.127.235.145:27017/Valholla_Testing?authSource=admin';
mongoose.connect(mongoDB);

mongoose.Promise = global.Promise;
var db = mongoose.connection;

module.exports = class PhaseManager {
    async updateDeploymentUrl(buildId, url) {
        console.log(url)
        console.log(buildId)
        await BuildSchema.update({
            _id: buildId
        }, {
                DeploymentUrl: url
            });
    }
    async updateBuild(buildId,instanceId) {
        if(instanceId !== false)
        await BuildSchema.findByIdAndUpdate(buildId, {InstanceId:instanceId, Status: "Completed", ModifiedDate: new Date()})
    } 
    
    async updatePhase(buildId, phaseName, phaseDetail) {

        await BuildPhaseSchema.update({
            _buildId: buildId,
            PhaseName: phaseName
        }, phaseDetail);
    }
    async getBuild(buildId) {
        var build = await BuildSchema.findById(buildId);
        return build;
    }
    async getProjectInfo(projectId) {
        var project = await ProjectSchema.findById(projectId)
        return project;
    }

    async getInstanceRegion(projectId) {
        var vmenv = await VMProjectEnvironmentConfigurationSchema.findOne({
            _projectId: projectId
        })
        var template = await VMTemplateDetailSchema.findById(vmenv._VMTemplateDetailId)
        var region = await RegionDetailSchema.findById(template._regionDetailId)
        return region.RegionCode;
    }

    async getEnvironmentConfig(environmentId) {
        // var vmenv = await VMProjectEnvironmentConfigurationSchema.findOne({
        //     _projectId: projectId
        // })
        var envConfiguration = await EnvironmentConfigureSchema.findById(environmentId)
        return envConfiguration;
    }
    
    async getEnvironmentProjectConfig(projectId) {
        var vmenv = await VMProjectEnvironmentConfigurationSchema.findOne({
            _projectId: projectId
        })
        return vmenv;
    }

}
