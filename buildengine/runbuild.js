var shell = require('shelljs');
const http = require('http');

var VMProjectEnvironmentConfigurationMaster = require('../models/VMProjectEnvironmentConfigurationSchema');
var userName = process.argv[3]
var fs = require('fs'); // file system
var PhaseManager = require("./phasemanager")
var buildId = process.argv[2]
var buildFolder = "./buildengine/buildout"
var projectName = "";
var giturl = ""
var versionIncr = ""
var srcFolderName = ""
var manager = new PhaseManager();
var scpclient = require('scp2')
var instanceIp = "";
var instanceId = "";
var regionCode = "";
node_ssh = require('node-ssh')
ssh = new node_ssh()
var envName = "";
var environmentConfig = null;
var projectInfo = null;
var environmentProjectConfig = null;
var build = null;
var environmentId = process.argv[4];
var isHavetoDeploy = process.argv[5];
var configFile = require('../config.json');

async function start() {
    build = await manager.getBuild(buildId);
    projectInfo = await manager.getProjectInfo(build._projectId);
    //console.log(projectInfo);
    environmentConfig = await manager.getEnvironmentConfig(environmentId)
    //console.log(environmentConfig);
    environmentProjectConfig = await manager.getEnvironmentProjectConfig(projectInfo._id)

   // console.log(environmentProjectConfig);

    projectName = projectInfo.ProjectName;
    versionIncr = build.BuildVersionType;
    srcFolderName = buildId + "_" + projectName
    envName = environmentConfig.EnvironmentName;
    srcFolderName =  srcFolderName.replace(/ /g,"_");
    console.log("Build starting for " + projectName)

    regionCode = await manager.getInstanceRegion(projectInfo._id);

    giturl = "https://gitlab.com/gmi-public/demosample.git" //get from repo manag

    
    if(isHavetoDeploy == 'true' || isHavetoDeploy){
        console.log("Creating VM...")
        instanceId = await createVM();
        console.log("VM Created")
          
        await manager.updateBuild(build._id,instanceId);        
    
    }

    await runPhase("GITPULL", () => {
        shell.cd(buildFolder)
        clean();
        gitClone();
    });
    await runPhase("CODEBUILD", async () => {
        await buildProject();
        console.log("CODEBUILD");
    });

    await runPhase("UNITTEST", async () => {
        await unitTest();
        console.log("UNITTEST");
    });

    await runPhase("SECURITYTEST", async () => {
        await securityTest();
        console.log("SECURITYTEST");
    });
    if(isHavetoDeploy == 'true' || isHavetoDeploy){
    await runPhase("DEPLOYMENT", async () => {
        await gitPush();
        await deploy();
        await manager.updateDeploymentUrl(buildId, `http://${instanceIp}:3000/`)
    });
    await runPhase("UFT", async () => {
        await uft();
    });
    await runPhase("COMPLETE", () => {

    });
    console.log("Waiting...")
    }
}


function gitPush() {
    shell.exec(`git add .`);
    shell.exec(`git commit -m "Auto-commit-${projectName}-${buildId}"`);
    //shell.exec(`git push`);
    shell.exec(`git push https://gmisa:welcome%4012@gitlab.com/gmi-public/demosample.git`);
}

function unitTest() {
    shell.exec("npm run unit-test");
}
function securityTest() {
    shell.exec("npm run sec-test");
}
function uft() {
    shell.exec("npm run uft-test");
}

function buildProject() {
    shell.cd(srcFolderName)
    if (versionIncr.toUpperCase() == "MAJOR") shell.exec("npm version major")
    else if (versionIncr.toUpperCase() == "MINOR") shell.exec("npm version minor")
    else if (versionIncr.toUpperCase() == "PATCH") shell.exec("npm version patch")
    shell.exec("npm install")
}


function clean() {
    return shell.rm('-rf', srcFolderName);

}

function gitClone() {
    return shell.exec(`git clone ${giturl} ${srcFolderName}`);
}

async function runPhase(phaseName, func) {
    await manager.updatePhase(buildId, phaseName, {
        PhaseStartTime: new Date(),
        Progress: 0,
        Status: "In Progress",
    });

    var out = await func();

    await manager.updatePhase(buildId, phaseName, {
        PhaseEndTime: new Date(),
        Progress: 100,
        Status: "Completed",
        PhaseLog: out
    });
}

function sleep(time) {
    var stop = new Date().getTime();
    while (new Date().getTime() < stop + time) {
        ;
    }
}

async function deploy() {
    console.log("Deploying...")
    var start = new Date().getTime();
    while (new Date().getTime() < (start + (120 * 1000))) {
        try {
            sleep(10000);
            console.log("Deploying...")
            instanceIp = await getInstanceIp(instanceId)

            var ssh = new node_ssh()
            console.log(instanceIp)

            await ssh.connect({
                host: instanceIp,
                username: 'ubuntu',
                privateKey: '../../deploy_key.pem'
            });

            out = await ssh.execCommand("sudo npm install forever -g")
	        console.log(out)

            out = await ssh.execCommand(`git clone ${giturl} ${srcFolderName}`)
            console.log(out)
            out = await ssh.execCommand(`npm install`, {
                cwd: `${srcFolderName}`
            })
            console.log(out)
            out = await ssh.execCommand('forever start src/app.js', {
                cwd: `${srcFolderName}`
            })
            console.log(out)
            out = await ssh.execCommand(`echo "Done"`)
            ssh.dispose();
            console.log(out)
            console.log("Done")
            return;
        } catch (ex) {
            console.log("VM Not yet started... Waiting" + ex)
            sleep(5000);
        }
    }
    console.log(out)
    console.log("Done")
}

async function getInstanceIp(instanceId) {

    var AWS = require('aws-sdk');
    // AWS.config.update({
    //     accessKeyId: "AKIAITAAUVFATOV3ICIA",
    //     secretAccessKey: "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",
    //     "region": regionCode
    // });

    AWS.config.update({
        accessKeyId: configFile.accessKeyId,
        secretAccessKey: configFile.secretAccessKey,
        "region": regionCode
    });

    var ec2 = new AWS.EC2();
    var params = {
        InstanceIds: [instanceId]
    };

    var result = await ec2.describeInstances(params).promise();
    var ip = result.Reservations[0].Instances[0].PublicIpAddress;
    return ip;
}

start().then(p => {
    console.log("Completed");
    process.exit(1);
})

async function createVM() {

    if(isHavetoDeploy == 'false' || !isHavetoDeploy){
        return false;
    }else{
    var AWS = require('aws-sdk');
    console.log(regionCode)
    AWS.config.update({
        accessKeyId: configFile.accessKeyId,
        secretAccessKey: configFile.secretAccessKey,
        "region": regionCode
    });

    var ec2 = new AWS.EC2({
        apiVersion: '2016-11-15'
    });

    var images = {
        "us-east-1": "ami-6628c01b",
        "ap-south-1": "ami-6628c01b",
        "ap-southeast-1": "ami-c2de96be"

    }

    var params = {
        ImageId: images[regionCode],
        InstanceType: 't2.micro',
        MinCount: 1,
        MaxCount: 1,
        KeyName: configFile.KeyName
    };

    var tagName = projectName + " " + envName

    var data = await ec2.runInstances(params).promise();
    console.log(data)
    var instanceId = data.Instances[0].InstanceId;
    var instancedetails = data.Instances[0];
    console.log("Created instance", instanceId);


    params = {
        Resources: [instanceId],
        Tags: [{
            Key: 'Name',
            Value: tagName
        }]
    };

    await ec2.createTags(params).promise();
    var objAssign = {
        _projectId: projectInfo._id,
        _cloudEnvironmentId: environmentProjectConfig._cloudEnvironmentId,
        _VMTemplateDetailId: environmentProjectConfig._VMTemplateDetailId,
        _environmentConfigureId: environmentConfig._id,
        InstanceId: instanceId,
        InstanceStatus: 'running',
        IsActive: true,
        CreatedBy: userName,
        ModifiedBy: null,
        CreatedDate: new Date(),
        ModifiedDate: null
    };

    await VMProjectEnvironmentConfigurationMaster.create(objAssign);
    console.log("Instance created successsfully with Instance ID - " + instanceId);
    return instanceId;    
}
}
